#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>

#define PT20_R_ZERO_mOhm		19960		// 19.96 * 1000
#define PT20_FACTOR				113			// 0.113 * 1000
#define PT20_IREF_uA			12500		// 12.5mA
#define VREF_uV					1100000UL
#define LSB_uV					(VREF_uV/1024)

// formula is RZERO + (FACTOR * T), output is °C
/* calculation by fixed point arithmetics
 * function returns temperature 
 */
uint16_t pt20_fixp32(uint16_t adcval) {
	uint32_t pt20_pV = (adcval * LSB_uV * 1000);
	uint32_t pt20_mr = pt20_pV / PT20_IREF_uA;
	uint32_t pt20_t_degc = (pt20_mr - PT20_R_ZERO_mOhm) / PT20_FACTOR;
	return (uint16_t)pt20_t_degc;
}

/* calculation by floating point arithmetics
 * function returns temperature in °C
 */
double pt20_double(uint16_t adcval) {
	double vref = 1.1;		// 1.1V
	double iref = 0.0125;	// 12.5mA current through pt20 (TL431 current source with 200R)
	double r0 = 19.96;		// 19.96Ohm, resistance at zero °C
	double f = 0.113;		// factor defined by formula
	int adc_steps = 1024;	// 10bit ADC=1024, 8bit ADC=256
	double adc_lsb = vref / adc_steps;
	
	// first calculate pt20_voltage (V)
	double pt20_voltage = adc_lsb * adcval;
	
	// because of U=R*I and our known reference current 
	// we can calculate resistance of PT20 sensor (Ohm)
	double pt20_r = pt20_voltage / iref;
	
	// we also calculate power dissipation in our sensor (W)
	double pt20_p = pt20_voltage * iref;
	
	// formula is RZERO + (FACTOR * T), output is °C
	double pt20_t = (pt20_r - r0) / f;
	
	return pt20_t;
}


int main(int argc, char *argv[]) {
	uint16_t adc;
	
	
	for(adc = 0; adc < 1024; adc++) {
			printf("%4u\t%.2f\t%u\n", adc, pt20_double(adc), pt20_fixp32(adc));
	}
	//for(adc = 0; adc < 1024; adc+=32) {
	//		pt20_fixp(adc);
	//}
	
	return 0;
}