#include <stdio.h>
#include <inttypes.h>


/* ADC STUFF */
#define ADC_SCALER_HAS_DIODE
#ifdef ADC_SCALER_HAS_DIODE
#define ADC_DIODE_DROP_uV       700000
#endif // ADC_SCALER_HAS_DIODE

#define ADC_EXTR_SCALE_RH	    10000
#define ADC_EXTR_SCALE_RL	    1000

#define ADC_RESOLUTION		    1024
#define ADC_REF_mV			    3300
#define ADC_REF_uV              (ADC_REF_mV*1000)
#define ADC_LSB_uV              (ADC_REF_uV/ADC_RESOLUTION)
#define ADC_DDR                 DDRB
#define ADC_PORT			    PORTB
#define ADC_PIN				    PINB
#define ADC_HWPIN			    PB3
#define ADC_AVERAGING_SAMPLES   50  // max 63 samples


int main(){
	uint32_t bat_voltage_uV = 0;
	uint32_t adc_voltage_uV = 0;
	uint32_t bat_voltage_mV = 0;
	uint32_t adcval = 250;
    uint8_t i;


    /* now calculate ADC input voltage (�V) */
    adc_voltage_uV = (ADC_LSB_uV * adcval);

    /* ADC input voltage is scaled down from battery voltage, multiply by resistor ratio.
     * results in real battery voltage (�V) minus diode drop
     */
    bat_voltage_uV = adc_voltage_uV * ((ADC_EXTR_SCALE_RH+ADC_EXTR_SCALE_RL)/ADC_EXTR_SCALE_RL);

    /* add diode drop to bat_voltage_�V */
    bat_voltage_uV += ADC_DIODE_DROP_uV;
    
    /* scale �V to mV */
    bat_voltage_mV = bat_voltage_uV / 1000;

	printf("adc_ref_uV = %luuV, 1 LSB = %luuV\n", ADC_REF_uV, ADC_LSB_uV);
	printf("adcval = %ulsbs\n",adcval);
	printf("adc voltage = %luuV\n",adc_voltage_uV);
	printf("diode drop = %luuV\n",ADC_DIODE_DROP_uV);
	printf("RH / RL = %u / %u\n",ADC_EXTR_SCALE_RH,ADC_EXTR_SCALE_RL);
	printf("resistor ratio = %u\n",((ADC_EXTR_SCALE_RH+ADC_EXTR_SCALE_RL)/ADC_EXTR_SCALE_RL));
	printf("bat voltage = %luuV, %lumV\n",bat_voltage_uV, bat_voltage_mV);
	printf("bat voltage = %lumV\n",bat_voltage_mV);
	
	

}
