from machine import I2C, Pin
import ssd1306
import time
import urandom
import utime

i2c = I2C(sda=Pin(4), scl=Pin(15))
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)

def reset_lcd():
    lcd_reset = Pin(16, Pin.OUT)
    lcd_reset.on()
    
    
def update_display():
    lcd.fill(0)
    
    unix=utime.time()
    tm_now=utime.localtime()
    h=tm_now[3]+1
    m=tm_now[4]
    s=tm_now[5]
    year=tm_now[0]
    month=tm_now[1]
    day=tm_now[2]
    dow=tm_now[6]+1
    doy=tm_now[7]+1

    str_h='%02d' % h
    str_m='%02d' % m
    str_s='%02d' % s
    tmpstr='+-- '+str_h+':'+str_m+':'+str_s+' --+'
    lcd.text(tmpstr, 0, 0)
    
    str_day='%02d' % day
    str_month='%02d' % month
    str_year='%02d' % year
    tmpstr='+- '+str_day+'.'+str_month+'.'+str_year+' -+'
    lcd.text(tmpstr, 0, 16)

    str_dow='Day of Week  %3d' % dow
    str_doy='Day of Year  %d' % doy
    lcd.text(str_dow, 0, 31)
    lcd.text(str_doy, 0, 40)
    tmpstr='UNIX = '+str(unix)
    lcd.text(tmpstr, 0, 54)

    lcd.show()

update_display()
while True:
    update_display()
