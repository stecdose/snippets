import network

cfg_ssid='Fuzzi'
cfg_password='fuzzifuzzi'

def do_connect():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(cfg_ssid, cfg_password)
        while not wlan.isconnected():
            pass
    else:
        print('Already connected!')
        
        
    print('network config:', wlan.ifconfig())

print('Helper for wireless connections...')
do_connect()