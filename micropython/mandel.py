from machine import I2C, Pin
import ssd1306

try:
    import micropython
except:
    pass

def mandelbrot():
    # returns True if c, complex, is in the Mandelbrot set
    #@micropython.native
    def in_set(c):
        z = 0
        for i in range(80):
            z = z*z + c
            if abs(z) > 60:
                return False
        return True

    lcd.fill(0)
    for u in range(127):
        for v in range(63):
            if in_set((u / 50 - 2) + (v / 32 - 1) * 1j):
                lcd.pixel(u, v,1)
                lcd.show()

# # PC testing
# import lcd
# lcd = lcd.LCD(128, 32)
i2c = I2C(sda=Pin(4), scl=Pin(15))
lcd_reset = Pin(16, Pin.OUT)
lcd_reset.on()
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)
lcd.fill(0)
lcd.text('calculating...', 3, 3)
lcd.show()
mandelbrot()
