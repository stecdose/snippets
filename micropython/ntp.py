import time
import ntptime

ntptime.settime()
print('Got time/date via NTP, this is the result:')
print(time.localtime())