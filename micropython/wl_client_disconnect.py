import network

def do_disconnect():
    wlan = network.WLAN(network.STA_IF)
    if not wlan.isconnected():
        print('Already disconnected!')
    else:
        print('disconnecting ')
        wlan.disconnect()
    
do_disconnect()