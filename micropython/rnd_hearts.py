from machine import I2C, Pin
import ssd1306
import time
import urandom


i2c = I2C(sda=Pin(4), scl=Pin(15))

lcd_reset = Pin(16, Pin.OUT)
lcd_reset.on()
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)

#lcd.fill(0)
#lcd.text('main.py', 3, 3)
#lcd.show()

ICON = [
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [ 0, 1, 1, 0, 0, 0, 1, 1, 0],
    [ 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [ 0, 1, 1, 1, 1, 1, 1, 1, 0],
    [ 0, 0, 1, 1, 1, 1, 1, 0, 0],
    [ 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [ 0, 0, 0, 0, 1, 0, 0, 0, 0],
]

#lcd.fill(0) # Clear the display
#for y, row in enumerate(ICON):
#    for x, c in enumerate(row):
#        lcd.pixel(x, y, c)

#lcd.show()




def random_heart():
    xofs = urandom.getrandbits(8)
    yofs = urandom.getrandbits(6)
    for y, row in enumerate(ICON):
        for x, c in enumerate(row):
            if c:
                lcd.pixel(x + xofs, y + yofs, c)

for n in range(100):
    random_heart()

lcd.show()



