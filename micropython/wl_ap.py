import network



ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid='LaufSchrift')
ap.config(authmode=3)
ap.config(password='123456789')

while ap.active() == False:
  pass

print('Created AP \'LaufSchrift\'')
print('Password \'123456789\'')
print('')
print(ap.ifconfig())