# This file is executed on every boot (including wake-boot from deepsleep)
import esp
esp.osdebug(0)
#import webrepl
#webrepl.start()

# init display and print message
from machine import Pin
from machine import I2C, Pin
import ssd1306
import time
import gc
import sys
from machine import Pin
import machine

i2c = I2C(sda=Pin(4), scl=Pin(15))
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)

machine.freq(160000000)

led_white = Pin(25, Pin.OUT)    # create output pin on GPIO0
led_white.off()                  # set pin to "on" (high) level

def toggle(p):
    p.value(not p.value())

def reload(mod):
    mod_name = mod.__name__
    del sys.modules[mod_name]
    gc.collect()
    return __import__(mod_name)

display_reset = Pin(16, Pin.OUT)
display_reset.on()

i2c = I2C(sda=Pin(4), scl=Pin(15))
#i2c.scan()

display = ssd1306.SSD1306_I2C(128, 64, i2c)
display.fill(0)

display.pixel(0, 0, 1)
display.pixel(127, 0, 1)
display.pixel(127, 63, 1)
display.pixel(0, 63, 1)
display.text('System BOOT OK!', 3, 9)

ICON = [
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [ 0, 1, 1, 0, 0, 0, 1, 1, 0],
    [ 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [ 0, 1, 1, 1, 1, 1, 1, 1, 0],
    [ 0, 0, 1, 1, 1, 1, 1, 0, 0],
    [ 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [ 0, 0, 0, 0, 1, 0, 0, 0, 0],
]

#display.fill(0) # Clear the display
for y, row in enumerate(ICON):
    for x, c in enumerate(row):
        display.pixel(110+x, 50+y, c)

display.show()

