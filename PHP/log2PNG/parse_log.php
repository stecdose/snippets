<?
	$source = 'sine_8bit.log';
	$output_png = 'output_8bit.png';
	$img_h = 256;

	$log_entries = 0;

	$file = fopen($source, "r");
	while(!feof($file)) {
		$entry = fgets($file, 16);
		//echo $entry;
		$log_entries++;
	}
	fclose($file);
	$img_w = $log_entries;
	echo "source file has $log_entries entries \n";
	
	$file = fopen($source, "r");

	$img = imagecreate($img_w, $img_h);
	$col_white = imagecolorallocate($img, 255, 255, 255);
	$col_gray  = imagecolorallocate($img, 192, 192, 192);
	$col_black = imagecolorallocate($img, 0,   0,   0  );
	$col_red   = imagecolorallocate($img, 255, 0,   0  );
	$col_blue  = imagecolorallocate($img, 0,   0,   255);
	$col_dgrn  = imagecolorallocate($img, 20,  200,  20);
	// imageline (im, x1, y1, x2, y2, col)

	// border
	$col_border = $col_black;
	imageline($img, 0, 0, 0, $img_h, $col_border);			// top
	imageline($img, 0, 0, $img_w, 0, $col_border);			// left
	imageline($img, $img_w-1, 0, $img_w-1, $img_h-1, $col_border);	// right
	imageline($img, 0, $img_h-1, $img_w-1, $img_h-1, $col_border);	// bottom

	// raster x
	$col_raster = $col_gray;
	$raster_row_x = 50;
	while($raster_row_x < $img_w) {
		imageline($img, $raster_row_x, 0, $raster_row_x, $img_h, $col_raster);
		$raster_row_x += 50;
	}	
	

	// raster y
	$col_raster = $col_gray;
	$raster_line_y = 50;
	while($raster_line_y < $img_h) {
		imageline($img, 0, $raster_line_y, $img_w, $raster_line_y, $col_raster);
		$raster_line_y += 50;
	}	


	// zero line
	$col_zero_line = $col_dgrn;
	imageline($img, 0, $img_h/2, $img_w, $img_h/2, $col_zero_line);
	

	$col_values = $col_red;

	$log_entries = 0;

	while(!feof($file)) {
		$entry = fgets($file, 16);
		$value = (int) ( $entry );
		//printf("value=%0.5f  y(value)=%d  x(time)=%d\n", $entry, $value, $log_entries);
		imagesetpixel($img, $log_entries, $value, $col_values);

		$log_entries++;
	}
	fclose($file);
	
	imagepng($img, $output_png);
	imagedestroy($img);

?>
