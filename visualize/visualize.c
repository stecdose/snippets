/* 2012-2013,2017 Nils Stec <stecdose@gmail.com> 
 *
 * v0.0 - first working tool
 * v0.1 - 
 * v0.1rc - almost on the way to 0.2
 *
 * */

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_gfxPrimitives.h>

#define NONE    0

#define THREADS_STOPPED         0
#define THREADS_RUNNING         1
#define THREADS_SLEEPING        2

// prototypes
extern int x11_screenres(int *x, int *y);
int exit_vis();
void init_vis();

// UI functions
void draw_meter_log_skeleton(int x_off, int y_off);
void draw_button_bar();
void draw_status_bar();

void update_button_bar();
void update_status_bar();
void update_clock();


// global stuff
SDL_Surface *surface;
struct global_configuration {
        char port[128];
        uint32_t baudrate;
        int databits;
        int parity;
        int stopbits;
        int connected;
        
        uint32_t device_address;
        uint32_t device_id;
        
        char logfile_ch0[128];
        char logfile_ch1[128];
        
        int32_t x11win_width;
        int32_t x11win_height;
        
        int thread_status;
} conf;


void init_vis() {
        conf.thread_status = THREADS_STOPPED;
        
        sprintf(conf.port, "/dev/ttyUSB0");
        conf.baudrate = 115200;
        conf.databits = 8;
        conf.parity = NONE;
        conf.stopbits = 1;
        conf.connected = 0;        
        conf.device_address = 0;
        conf.device_id = 0x73537353;

		x11_screenres(&conf.x11win_width,&conf.x11win_height);
		printf("Xlib reported screen resolution: %d %d\n", conf.x11win_width, conf.x11win_height);        
		
		return;        
}

int main(int argc, char **argv) {
		uint8_t key;
		SDL_Event event;
        
        init_vis();
    
		SDL_Init(SDL_INIT_VIDEO);
		surface = SDL_SetVideoMode(1024, 768, 16, SDL_HWSURFACE | SDL_DOUBLEBUF);

		SDL_WM_SetCaption(" NOT CONNECTED. < WAITING > ", NULL);

		while(1) {
			while(SDL_PollEvent(&event)) {
				switch(event.type) {
					case SDL_KEYDOWN:			// key pressed
						key = event.key.keysym.sym;
						switch(key) {			
							case SDLK_ESCAPE:
								exit_vis();
								break;
							case SDLK_RETURN:
								break;
						}
						break;
					case SDL_QUIT:
						exit_vis();
						break;
				}
			}        
        
        draw_meter_log_skeleton(100, 160);
        draw_meter_log_skeleton(100, 460);
        draw_status_bar();
        update_status_bar();
        
        draw_button_bar();
        update_button_bar();
         
        SDL_UpdateRect (surface, 0, 0, 1023, 767);
	}

	SDL_FreeSurface(surface);
	SDL_Quit();
	return 0;  
}

void draw_status_bar() {
        uint8_t sb_r = 0x00;
        uint8_t sb_g = 0xff;
        uint8_t sb_b = 0xff;
        uint8_t sb_a = 0xff;

        uint8_t border_r = 0xff;
        uint8_t border_g = 0x00;
        uint8_t border_b = 0x00;
        uint8_t border_a = 0xff;
        
        boxRGBA(surface, 0, 747, 1022, 766, sb_r, sb_g, sb_b, sb_a); 
        rectangleRGBA(surface, 0, 747, 1022, 766, border_r, border_g, border_b, border_a);
}

void update_status_bar() {

}

void draw_meter_log_skeleton(int x_off, int y_off) {
        int size_x = 900;
        int size_y = 256;
        
        uint8_t border_r = ~0x12;
        uint8_t border_g = ~0x34;
        uint8_t border_b = ~0x56;
        uint8_t border_a = 0xff;
        
        uint8_t zeroline_r = 0xff;
        uint8_t zeroline_g = 0x00;
        uint8_t zeroline_b = 0xff;
        uint8_t zeroline_a = 0xff;
        
        uint8_t divline_r = 0xff;
        uint8_t divline_g = 0x00;
        uint8_t divline_b = 0xff;
        uint8_t divline_a = 0xff;
        
        
        // channel log box
        rectangleRGBA(surface, x_off, y_off, x_off+size_x+2, y_off+size_y+2, border_r, border_g, border_b, border_a);
        // box below channel log, infobox
		rectangleRGBA(surface, x_off, y_off+size_y+2, x_off+size_x+2, y_off+size_y+2+20, border_r, border_g, border_b, border_a);
        // zero lines
        lineRGBA(surface, x_off+1, y_off+(size_y/2), x_off+size_x+1, y_off+(size_y/2), zeroline_r, zeroline_g, zeroline_b, zeroline_a);
        lineRGBA(surface, x_off+(size_x/2), y_off+1, x_off+(size_x/2), y_off+size_y, zeroline_r, zeroline_g, zeroline_b, zeroline_a);
        
        // divisions from 0 to right border
        int div_space = 30;			// space between two division line
		int start_x = x_off+div_space; //+(size_x/2);		
		int div_line_height = 16;	// height of a division line
		int y = (y_off+(size_y/2))-(div_line_height/2);
		int x;
		
		for(x = start_x; x < (x_off+size_x); x += div_space) {
			lineRGBA(surface, x, y, x, y+div_line_height, divline_r, divline_g, divline_b, divline_a);
		}
		
		// divisions from 0 to bottom border
		int start_y = y_off+8;
		div_space = 30;
		int div_line_width = 16;
		x = x_off+(size_x/2)-(div_line_width/2);
		for(y = start_y; y <= (y_off+size_y); y += div_space) {
			lineRGBA(surface, x, y, x+div_line_width, y, divline_r, divline_g, divline_b, divline_a);
		}
        

        
        
        
}

void draw_button_bar() {
		uint8_t bb_r = 0x00;
        uint8_t bb_g = 0xff;
        uint8_t bb_b = 0xff;
        uint8_t bb_a = 0xff;

        uint8_t border_r = 0xff;
        uint8_t border_g = 0x00;
        uint8_t border_b = 0x00;
        uint8_t border_a = 0xff;
        
        boxRGBA(surface, 0, 0, 1022, 20, bb_r, bb_g, bb_b, bb_a); 
        rectangleRGBA(surface, 0, 0, 1022, 20, border_r, border_g, border_b, border_a);        
        
        stringRGBA(surface, 8, 7, "[F1] connection setup  [F2] screenshot  [F5] START/STOP   [F8] SINGLE/DUAL CHANNEL                                  [ESC] QUIT", 0xff, 0x3f, 0xae, 0xff);
}

void update_button_bar() {
        
}

int exit_vis() {
        printf("visualize exits now...\n");
    
        printf("closing all connections [serial...]\n");
        /* close_serial_con(); */
    
        printf("freeing SDL surfaces, exiting SDL...\n");
		SDL_FreeSurface(surface);
		SDL_Quit();
    
        printf("out here.\n");
		
		
		exit(0);
}
