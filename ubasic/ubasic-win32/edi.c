#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "edi.h"

static int term_sizex = 80;
static int term_sizey = 25;

#define _printf                     printf
#define _puts                       puts
#define _putc(c)		    putc(c,stdout)
#define ansi_clrscr()               _puts("\x1B[2J\x1B[H")
#define ansi_home()                 _puts("\x1B[H")
#define ansi_gotoyx(y,x)            _printf("\x1B[%u;%uH",1+y,1+x)
#define ansi_nl()                   _putc('\n')

#define ansi_attr_bold()            _puts("\x1B[1m")
#define ansi_attr_dim()             _puts("\x1B[2m")
#define ansi_attr_reset()           _puts("\x1B[m")
#define ansi_attr_underline()       _puts("\x1B[4m")
#define ansi_attr_blink()           _puts("\x1B[5m")
#define ansi_attr_reverse()         _puts("\x1B[7m")
#define ansi_attr_invisible()       _puts("\x1B[8m")

#define ansi_setwinyy(y1,y2)        _printf("\x1B[%u;%ur",y1+1,y2+1)        // line;liner   set top+bottom line of win, DECSTBM
#define ansi_movecsr_up(lines)      _printf("\x1B[%uA",lines)
#define ansi_movecsr_down(lines)    _printf("\x1B[%uB",lines)
#define ansi_movecsr_right(cols)    _printf("\x1B[%uC",cols)
#define ansi_movecsr_left(cols)     _printf("\x1B[%uD",cols)
#define ansi_scroll_1up()           _puts("\x1BD")
#define ansi_scroll_1down()         _puts("\x1BM")
#define ansi_nextline()             _puts("\x1BE")
#define ansi_savecsrattr()          _puts("\x1B7")
#define ansi_restorecsrattr()       _puts("\x1B8")



void edi(void) {
    int x, y;
    ansi_clrscr();

    for(y = 0; y < (term_sizey-1); y++) {
        for(x = 0; x < term_sizex; x++) {
            ansi_gotoyx(y,x);
            _putc(".");
        }
    }

    ansi_gotoyx(25,0);
    puts("STATUSLINE");
    ansi_gotoyx(0,0);

    getc(stdin);
    exit(0);
    return;
}

