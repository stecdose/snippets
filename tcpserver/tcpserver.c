#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <signal.h>
#include <errno.h>

#define TCP_PORT	15000

int main_socket, connection_socket;

void sigint_callback(int sig_num);  

void sigint_callback(int sig_num) {
    signal(SIGINT, sigint_callback);
    close(connection_socket);
    close(main_socket);
    printf("exit\n");
    exit(-1);
}  

int main(int argc, char* argv[]) {
	socklen_t len = sizeof(struct sockaddr);
	char txbuf[256], rxbuf[256];	
	struct sockaddr_in sa_server, sa_client;
	int connected, rv;

    signal(SIGINT, sigint_callback);

	main_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(main_socket < 0)	{
		printf("Error creating main_socket\n");
		printf("'%s'\n", strerror(errno));
		exit(-1);
	}

	if(setsockopt(main_socket, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0) {
		printf("Error setsockopt()\n");
		printf("'%s'\n", strerror(errno));
		exit(-1);
	}
	
	sa_server.sin_family = AF_INET;				// IPv4
	sa_server.sin_addr.s_addr = INADDR_ANY;		// we accept from anyone
	sa_server.sin_port = htons(TCP_PORT);
	memset(&(sa_server.sin_zero), 0, 8);
	
	if(bind(main_socket, (struct sockaddr *)&sa_server, sizeof(struct sockaddr)) < 0) {
      printf("Error in bind()\n");
      printf("'%s'\n", strerror(errno));
      close(main_socket);
      exit(-1);
	}

	if(listen(main_socket, 5) < 0) {
      printf("Error in listen()\n");
      printf("'%s'\n", strerror(errno));
      close(main_socket);
      exit(-1);
	}

	while(1) {
		/* wait for client connections */
		printf("listening on port %d\n", TCP_PORT);
		if((connection_socket = accept(main_socket, (struct sockaddr *) &sa_client, &len)) < 0) {
			printf("Error in accept() ");
			printf("'%s'\n", strerror(errno));
			close(main_socket);
			close(connection_socket);
			exit(-1);
		}
		connected = 1;
		printf("accepted connection from %s\n", inet_ntoa(sa_client.sin_addr));
		
		/* show welcome msg */
		bzero(txbuf, sizeof(txbuf));
		sprintf(txbuf, "welcome to bla @ blubb\n");
		send(connection_socket, txbuf, strlen(txbuf), 0);

		while(connected) {
			bzero(rxbuf, sizeof(rxbuf));
			ret = recv(connection_socket, rxbuf, sizeof(rxbuf), 0);
			if(ret <= 0) {
				connected = 0;
				printf("client has closed connection.\n");
			} else {
				printf("got message (%dbytes).\n", ret);
				// here do something with reveived message		
			}
		}
		/* client has disconnected */
		close(connection_socket);
	}
}
