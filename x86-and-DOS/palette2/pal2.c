#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <graph.h>
#include <dos.h>
#include <inttypes.h>


void fill_scr_rand(void);
int gfx_mode(uint8_t mode);

/* this is our source for random numbers 
 * WAAAAY faster than libc's random 
 */
uint32_t xorshift32() {
    static uint32_t x = 1;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    return x;
}

int gfx_mode(uint8_t mode) {
    union REGS regs;
    
    regs.h.ah = 0x00;
    regs.h.al = mode;
    int86(0x10, &regs, &regs);
    
    return 0;
}


int main() {
    int c = 0, run = 1; 
    
    srand(time(NULL));

    gfx_mode(0x13);
    fill_scr_rand();
    
    while(run) {
        if(!kbhit()) {
            delay(50);
            rand_palette();
        } else {
            c = getch();
            switch(c) {
                case ' ':
                    getch();
                    break;
                case 'r':
                    break;
                case 'q':
                    run = 0;
                    break;
            }
        }
    }
    gfx_mode(0x03);
    return 0;
}

void fill_scr_rand(void) {
    union REGS regs;
    int i, r, x, y, c;
    
    regs.h.ah = 0x0C;   /* function: PLOT PIXEL */
    
    for(i = 0; i < 2000; i++) {
        x = (uint16_t)(xorshift32() % 320);
        y = (uint16_t)(xorshift32() % 200);
        c = (uint8_t)xorshift32();
        regs.h.al = c;
        regs.x.cx = x;
        regs.x.dx = y;
        int86(0x10, &regs, &regs);
    }

    return;    
}

void rand_palette(void) {
    int i;
    uint32_t r;
    
    /* get a random value */
    
    
    /* wait for retrace signal */
    while((inp(0x03DA) & 0x08));
    while(!(inp(0x03DA) & 0x08));
     
    outp(0x03C8, 1);
    
    /* write 15 new values for 15 colors */
    for(i=0; i <= 254; i++) { 
      r = xorshift32();
      outp(0x03C9, (uint8_t)r);
      outp(0x03C9, (uint8_t)(r>>4));
      outp(0x03C9, (uint8_t)(r>>8));        
    }
}
