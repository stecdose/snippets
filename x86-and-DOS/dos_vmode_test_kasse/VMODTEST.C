/*	HELLO.C -- Hello, world */

#include <stdio.h>
#include <dos.h>

void main()
{
	unsigned short ax, bx, cx, dx;
	unsigned char int_n;

	puts("enter AX,BX,CX,DX (4x 4 hex digits) . . . \n");
	puts("AX=0x");
	scanf("%x", &ax);
	puts("BX=0x");
	scanf("%x", &bx);
	puts("CX=0x");
	scanf("%x", &cx);
	puts("DX=0x");
	scanf("%x", &dx);

	puts("enter int number (2 hex digits) . . .\n");
	puts("INT=0x");
	scanf("%x", &int_n);

	_AX = ax;
	_BX = bx;
	_CX = cx;
	_DX = dx;

	geninterrupt(int_n);

	ax = _AX;
	bx = _BX;
	cx = _CX;
	dx = _DX;

	puts("registers after interrupt:\n");
	printf("INT=0x%02x  AX=0x%04x BX=0x%04x CX=0x%04x DX=0x%04x\n", int_n, ax, bx, cx, dx);

	puts("press a key to exit...\n");

	getch();




}



