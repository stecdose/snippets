#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <dos.h>
#include <inttypes.h>

void fill_scr_rand(void);

void rand_palette(void) {
    int i=0;
    int r;
    
    /* get a random value */
    r = rand();
    
    /* wait for retrace signal */
    while((inp(0x03DA) & 0x08));
    while(!(inp(0x03DA) & 0x08));
    
    /* we start at index 1, we leave BLACK (0) the way it is */
    outp(0x03C8, 1);
    
    /* write 15 new values for 15 colors */
    for(i=0; i<15; i++) { 
      outp(0x03C9, (uint8_t)r);
      outp(0x03C9, (uint8_t)(r>>4));
      outp(0x03C9, (uint8_t)(r>>8));        
    }
}

int main() {
    int c = 0, run = 1; 
    int x, y;
    
    srand(time(NULL));
    
    fill_scr_rand();
    
    while(run) {
        if(!kbhit()) {
            rand_palette();
            delay(50);
        } else {
            c = getch();
            switch(c) {
                case 'r':
                    fill_scr_rand();
                    break;
                case 'q':
                    run = 0;
                    break;
            }
        }
    }

    return 0;
}

void fill_scr_rand(void) {
    int i;
    int r;

    uint8_t far *vram;
    vram = MK_FP(0xB800, 0x0000);
    
    for(i = 0; i < 2000; i++) {
        r = rand();
        *vram = (uint8_t)r;
        vram++;
        *vram = (uint8_t)(r>>8&0x0F);
        vram++;
    }

    return;    
}
