#ifndef __VESA_H__
#define __VESA_H__
#include <inttypes.h>
#include <stdint.h>

#define MODE_NORMAL         3
#define MODE_640x480x256    0x101
#define MODE_800x600x256    0x103
#define MODE_1024x768x256   0x105
#define MODE_1280x1024x256  0x107

typedef struct _modeInfoBlock {
    uint16_t attributes;
    uint8_t windowA, windowB;
    uint16_t granularity;
    uint16_t windowSize;
    uint16_t segmentA, segmentB;
    uint32_t winFuncPtr;                      /* ptr to INT 0x10 Function 0x4F05 */
    uint16_t pitch;                           /* bytes per scan line */

    uint16_t resolutionX, resolutionY;        /* resolution */
    uint8_t wChar, yChar, planes, bpp, banks; /* number of banks */
    uint8_t memoryModel, bankSize, imagePages;
    uint8_t reserved0;

    uint8_t readMask, redPosition;            /* color masks */
    uint8_t greenMask, greenPosition; 
    uint8_t blueMask, bluePosition;
    uint8_t reservedMask, reservedPosition;
    uint8_t directColorAttributes;

    uint32_t physbase;                        /* pointer to LFB in LFB modes */
    uint32_t offScreenMemOff;
    uint16_t offScreenMemSize;
    uint8_t  reserved1 [206];
} mode_info_t;

typedef struct _vbeInfoBlock {
    uint8_t  signature[4];     // “VESA”
    uint16_t version;          // Either 0x0200 (VBE 2.0) or 0x0300 (VBE 3.0)
    uint32_t oemString;        // Far pointer to OEM name
    uint8_t  capabilities[4];  // capabilities
    uint32_t videoModesPtr;    // Far pointer to video mode list
    uint16_t totalMemory;      // Memory size in 64K blocks
    uint16_t oemSoftwareRev;
    uint32_t oemVenderNamePtr;
    uint32_t oemProductNamePtr;
    uint32_t oemProductRevPtr;
    uint8_t  reserved [222];
    uint8_t  oemData [256];
} vbe_info_t;

int init_vbe_driver(int mode);
int vbe_switch_mode(int mode);

void vbe_select_bank(int bank);

int vbe_setpx(int x, int y, uint8_t color);
int vbe_fill(uint8_t color);

#endif // __VESA_H__
