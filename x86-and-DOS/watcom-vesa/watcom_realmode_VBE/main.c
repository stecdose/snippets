#include <stdio.h>
#include <string.h>
#include <dos.h>
#include <graph.h>
#include <stdlib.h>

#include "gfxmenu.h"
#include "vesa.h"

int main(int argc, char **argv) {
    uint32_t i;
    int y;
    uint8_t color;

    if(init_vbe_driver(MODE_800x600x256) == -1) {
        printf("failed to init video driver!\n");
        exit(-1);
    }
//     vbe_fill(0xAB);

    color = 0x00;
    for(y = 0; y < 512; y+=2) {
        for(i = 0; i < 800; i++) {
            vbe_setpx(i, y, color);
            vbe_setpx(i, y+1, color);
        }
        color++;
    }

    return 0;
}

