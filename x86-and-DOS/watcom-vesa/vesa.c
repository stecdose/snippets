#include <stdio.h>
#include <string.h>
#include <dos.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>
#include "vesa.h"

static int vbe_detect(void);
static int vbe_update_global_modeinfo(int mode);

static vbe_info_t global_vbeinfo;
static mode_info_t global_modeinfo;

static int selected_bank;

uint8_t far *video_ptr;


/* returns:
 *  0  on success 
 * -1  if failed
 * -2  if VBE available, but mode not 
 */
int init_vbe_driver(int mode) {
    int x,y;

    if(!vbe_detect()) {
        printf("VESA BIOS EXTENSIONS NOT AVAILABLE!\n");
        return -1;
    }

    if(vbe_update_global_modeinfo(mode) == -1) {
        printf("mode 0x%03x not supported!\n", mode);
        return -2;
    }

    vbe_switch_mode(mode);

    return 0;
}

int vbe_fill(uint8_t color) {
    uint32_t s;

    for(s = 0; s < 7; s++) {
        vbe_select_bank(s);
        _fmemset(video_ptr, color, 0xffff);
    }
    vbe_select_bank(7);
    _fmemset(video_ptr, color, 22000);

    return 0;
}

/* returns 
 *    -1 on failure, coordinates out of range
 *     0 on success
 */
int vbe_setpx(int x, int y, uint8_t color) {
    uint32_t lin_addr, vram_off;
    int vram_bank;

    if((x >= 800)||(y >= 600)) {
        return -1;
    }

    lin_addr  = (y*((uint32_t)global_modeinfo.pitch))+x;
    vram_off  = lin_addr % ((uint32_t)global_modeinfo.granularity*1024UL);
    vram_bank = lin_addr >> 16;   // divide by 0x10000

    vbe_select_bank(vram_bank);
    video_ptr[vram_off] = color;

    return 0;
}

//~ int vbe_hline(int x1, int x2, int y, uint8_t color) {
    //~ uint32_t lin_addr;
    //~ int bank_start, bank_end, bank;

    //~ lin_addr  = (y*((uint32_t)global_modeinfo.pitch))+x1;
    //~ vram_off  = lin_addr % ((uint32_t)global_modeinfo.granularity*1024UL);
    //~ vram_bank = lin_addr >> 16;   // divide by 0x10000
    
//~ }

void vbe_select_bank(int bank) {
    union REGS r;

    if(bank == selected_bank) {
        return;
    }

    r.x.ax = 0x4F05;
    r.x.bx = 0;
    r.x.dx = bank;
    int86(0x10, &r, &r);
    selected_bank = bank;

    return;
}

/* returns 1 if VESA BIOS found, -1 if failed.
 * if success, fills global_vbeinfo with data for later use 
 */
static int vbe_detect(void) {
    union REGS r;
    struct SREGS s;

    r.x.ax = 0x4f00;
    r.x.di = FP_OFF(&global_vbeinfo);
    s.es = FP_SEG(&global_vbeinfo);
    int86x(0x10,&r,&r,&s);

    /* check, if VESA signature is found or not */
    if(strncmp(global_vbeinfo.signature, "VESA", 4)) {
        return -1;
    }

    return 1;
}

/* returns:
 *  0  on success
 * -1  if mode not supported
 */
int vbe_update_global_modeinfo(int mode) {
    union REGS r;
    struct SREGS s;

    /* get mode-info, check if mode is available */
    r.x.ax=0x4f01;
    r.x.cx=mode;
    r.x.di=FP_OFF(&global_modeinfo);
    s.es=FP_SEG(&global_modeinfo);
    int86x(0x10,&r,&r,&s);

    if(!(global_modeinfo.attributes & 0x01)) {
        return -1;
    }

    return 0;
}

/* returns:
 *  0  on success
 * -1  if mode not supported
 */
int vbe_switch_mode(int mode) {
    union REGS r;

    if(vbe_update_global_modeinfo(mode) == -1) {
        return -1;
    }

    /* switch mode */
    r.x.ax=0x4f02;
    r.x.bx=mode;
    int86(0x10,&r,&r);

    selected_bank = 0;
    video_ptr = MK_FP(0xA000, 0x0000);

    return 0;
}
