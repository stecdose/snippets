#include <stdio.h>
#include <conio.h>
#include <dos.h>

#include "con.h"

int main() {
    int x, y;
    uint8_t attr;
    char str[9];
    
    clrscr();
    
    for(x = 0; x < 16; x++) {
        for(y = 0; y < 16; y++) {
            attr = (uint8_t)((x << 4) | (y & 0x0f));
            snprintf(str, 9, " %02Xh ", attr);
            puts_attr(x*5, y, str, attr);
        }
    }
    
    bios_gotoxy(0, 20);
    
    return 0;
}

