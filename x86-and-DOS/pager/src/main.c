#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <graph.h>
#include <inttypes.h>
#include <dos.h>

#include "con.h"


/* these defines are used for screen layout and colors, it's a mess, but
 * far better than hardcoded these values all over the code...
 */
#define TOP_LINE_BG_CHAR    0xDB
#define TOP_LINE_BG_COLOR   0x04
#define TOP_LINE_TEXT_COLOR 0x4f

#define BOT_LINE_BG_CHAR    0xDB
#define BOT_LINE_BG_COLOR   0x04
#define BOT_LINE_TEXT_COLOR 0x4f
#define BOT_LINE_HL_COLOR   0x4a

#define BG_CHAR             0xB2
#define BG_COLOR            0x08

#define BORDER_COLOR        0x47

/* functions for text user interface + file handling stuff */
void draw_main_window(char *title);
void draw_pager_win(void);
uint32_t count_lines(void);
void display_line(int l, int y);
void display_scroll_one_up(void);
void clear_line(int line);

/* filename of displayed file */
char filename[129];

void display_line(int l, int y) {
    FILE *f;
    char line[256+1];
    
    f = fopen(filename, "r");
    /* file existent? */
    if(f == NULL) {
        clrscr();
        puts_attr(0, 0, " can't open file '", 0x0F);
        puts_attr(16+2, 0, filename, 0x0f);
        puts_attr(16+2+strlen(filename), 0, "' !!!", 0x0f);
        exit(-1);
        return;
    }
    
    /* skip lines if l > 0 */
    while(l > 0) {
            fgets(line, 256, f);
            l--;
    }
    
    /* now we have the line we want to display */
    fgets(line, 256, f);
    
    /* cut after 75 chars line length */
    line[76] = 0;
    
    /* display the line */
    puts_attr(2, y, line, 0x07);   
    
    fclose(f);   
    
    return;
}

/* we have a display area of 21 lines, display them all
 * arguments:
 *    int start_line    = first line of file to display
 *    filename is a global variable of type char[129] (128chars+\0)
 *             and used in display_line()
 */
void display_page(int start_line) {
    int y;
    for(y = 0; y < 21; y++) {
        display_line(y, y+2);
    }
}

/* move the whole display area one line up in text file, means move displayed stuff one line down */
void display_scroll_one_up(void) {
    uint8_t far *l0;
    uint8_t far *l1;
    
    int x, y, i, offset, c;
    
    /* move character line by line */
    x = 2;
    for(i = 0; i < 20; i++) {
        y = 22-i;
        
        offset = ((y*80)+x)*2;      /* destination */
        l0 = MK_FP(0xB800, offset);
        
        offset -= 80*2;             /* source */
        l1 = MK_FP(0xB800, offset);
        
        for(c = 0; c < 75*2; c++) { /* 75 chars a line, do it char by char */
            *l0 = *l1;
            l0++;
            l1++;
        }
    }
    
    /* clear first line */
    clear_line(0);
}

/* move the whole display area one line down in text file, means move displayed stuff one line up */
void display_scroll_one_down(void) {
    uint8_t far *l0;
    uint8_t far *l1;
    
    int x, y, i, offset, c;
    
    /* move characters line by line */
    x = 2;          // offset on screen X
    for(i = 0; i < 20; i++) {
        y = 2+i;        // offset on screen Y
      
        offset = ((y*80)+x)*2;         /* destination */
        l0 = MK_FP(0xB800, offset);
        
        offset += 80*2;                /* source */
        l1 = MK_FP(0xB800, offset);
        
        for(c = 0; c < 75*2; c++) { /* 75 chars a line, do it char by char */
            *l0 = *l1;
            l0++;
            l1++;
        }
    }
    
    /* clear last line */
    clear_line(20);
    
    return;
}

/* bottom bar shows the position in file + total lines */
// currently not enabled, (s)printf uses so much space. convert to ultoa!
void update_line_display(int start_line, int lines) {
//     int x, y;
//     char tmp[32];
//     
//     y = 24;
//     x = 30;
// //     putc_attr(x, y, '[', 0x48);
//     
//     x += 2;
// //     sprintf(tmp, "%d", start_line+1);
// //     puts_attr(x, y, tmp, 0x47);
//     
//     x += strlen(tmp);
// //     putc_attr(x, y, '-', 0x48);
//     
//     x += 1;
// //     sprintf(tmp, "%d", start_line+20+1);
// //     puts_attr(x, y, tmp, 0x47);
//     
//     x += strlen(tmp);
// //     puts_attr(x, y, " / ", 0x48);
//     
//     x += 3;
// //     sprintf(tmp, "%d", lines-1);
// //     puts_attr(x, y, tmp, 0x47);
//     
//     x += strlen(tmp)+1;
// //     putc_attr(x, y, ']', 0x48);
//     
//     x += 1;
//     while(x < 65) {
//         putc_attr(x, y, ' ', 0x44);
//         x++;
//     }
    
    return;
}

void main(int argc, char **argv) {
    uint8_t c;
    char title[51], tmp[32];
    uint32_t lines;
    int start_line = 0;
    
    init_con();
    
    clrscr();

    if(argc < 2) {
        gotoxy(7,0);
        puts_attr(0, 3, "                                               ", 0x4f);
        puts_attr(0, 4, " ERROR !                                       ", 0x4a|0x80);
        puts_attr(0, 5, "  usage: pager.com textfile.txt <window title> ", 0x4f);
        puts_attr(0, 6, "                                               ", 0x4f);
        exit(-1);
    }
    
    strncpy(filename, argv[1], 128);
    
    if(argc == 3) { /* we also got window title */
        strncpy(title, argv[2], 50);
    } else {        /* no window title? then we use filename as window title */
        strncpy(title, argv[1], 50);
    }
    
    draw_main_window(title);
    draw_pager_win();
    
    lines = count_lines();
    start_line = 0;
    
    update_line_display(start_line, lines);
    
    display_page(start_line);
    
    for(;;) {
        c = getch();
        switch(c) {
            case 0x48:  /* ARROW UP */
                if(start_line > 0) {
                    start_line--;
                    display_scroll_one_up();
                    display_line(start_line, 2);
                    update_line_display(start_line, lines);
                }
                break;
            case 0x50:  /* ARROW DOWN */
                if((start_line + 22) < lines) {
                    start_line++;
                    display_scroll_one_down();
                    display_line(start_line+20, 22);
                    update_line_display(start_line, lines);
                }
                break;
            case 0x49:  /* PAGE UP */
                break;
            case 0x51:  /* PAGE DOWN */
                break;
            case 0x44:  /* F10 */
                clrscr();
                gotoxy(24,0);
                puts_attr(2, 18, "                                 ", 0x70);
                puts_attr(2, 19, " TEXT-PAGER v0.00                ", 0x70);
                puts_attr(2, 20, "  (c) 2018 <stecdose@gmail.com>  ", 0x70);
                puts_attr(2, 21, "                                 ", 0x70);
                exit(0);
                break;
        }
    }
}

uint32_t count_lines(void) {
    FILE *f;
    int lines;
    char line[256+1];
    
    f = fopen(filename, "r");
    if(f == NULL) {
        puts_attr(0, 0, " can't open file '", 0x0F);
        puts_attr(16+2, 0, filename, 0x0f);
        puts_attr(16+2+strlen(filename), 0, "' !!!", 0x0f);        
        return 0;
    }
    
    lines = 0;
    while(fgets(line, 256, f)) {
        lines++;
    }
    
    fclose(f);
    
    return lines;
}

void draw_main_window(char *title) { 
    int i;
    uint8_t far *vram;
    char str[55] = { 0 };
    
    /* draw top border line */
    vram = MK_FP(0xB800, 0);
    for(i = 0; i < 80; i++) {
        *vram = TOP_LINE_BG_CHAR;
        vram++;
        *vram = TOP_LINE_BG_COLOR;
        vram++;
    }
    
    /* draw bottom border line */
    vram = MK_FP(0xB800, 80*2*24);
    for(i = 0; i < 80; i++) {
        *vram = BOT_LINE_BG_CHAR;
        vram++;
        *vram = BOT_LINE_BG_COLOR;
        vram++;
    }
    
    /* write title bar text, centered */
    i = (80-strlen(title))/2;
    strncat(str, "[ ", 2);
    strncat(str, title, 50);
    strncat(str, " ]", 2);
    puts_attr(i, 0, str, TOP_LINE_TEXT_COLOR); 

    /* write bottom bar text */
//     puts_attr(1, 24, " \x18/\x19 + PGUP/PGDN NAVIGATE                                            F10 QUIT", BOT_LINE_TEXT_COLOR);
    puts_attr(1, 24, " \x18/\x19 NAVIGATE                                                        F10 QUIT", BOT_LINE_TEXT_COLOR); 
    putc_attr(2, 24, 0x18, BOT_LINE_HL_COLOR);
    putc_attr(4, 24, 0x19, BOT_LINE_HL_COLOR);
    /*
      puts_attr(8,24, "PGUP", BOT_LINE_HL_COLOR);
      puts_attr(13,24, "PGDN", BOT_LINE_HL_COLOR);
    */
    puts_attr(70,24, "F10", BOT_LINE_HL_COLOR);
    
    /* fill background with char and attribute */
    vram = MK_FP(0xB800, 80*2);
    for(i = 0; i < (80*23); i++) {
        *vram = BG_CHAR;
        vram++;
        *vram = BG_COLOR;
        vram++;
    }
    
    return;
}

void draw_pager_win(void) {
    uint8_t far *vram;
    
    int x, y, offset;
    
    /* draw textbox */
    for(y = 2; y < 23; y++) {
        offset = (y*80+2)*2;
        vram = MK_FP(0xB800, offset);
        for(x = 0; x < 76; x++) {
            *vram = ' ';
            vram++;
            *vram = 0x07;
            vram++;
        }
    }
    
    /* draw scroll bar */
    for(y = 3; y < 22; y++) {
        offset = (y*80+77)*2;
        vram = MK_FP(0xB800, offset);
        *vram = 0xB1;
        vram++;
        *vram = 0x4A;
    }
    
    /* draw arrows on scroll bar */
    putc_attr(77, 2, 0x1e, 0x4A);
    putc_attr(77, 22, 0x1f, 0x4A);
    
    return;
}

void clear_line(int line) {
    char far *vram;
    int i, x, y, offset;
    
    x = 2;
    y = 2+line;
    offset = ((y*80)+x)*2;
    vram = MK_FP(0xB800, offset);
    for(i = 0; i < 75; i++) {
        *vram = ' ';
        vram++;
        *vram = 0x07;
        vram++;
        
    }
    return;    
}
