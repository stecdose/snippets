/*  x86 512byte C pong - real mode
 * 
 *  runs on bare metal hardware, it just uses some BIOS
 *  interrupts.
 * 
 *  (c) 2018 <stecdose@gmail.com>
 */

/* datatypes */
#define uint8_t		unsigned char
#define uint16_t 	unsigned short
#define NULL 		((void *)0)

void _puts(char *str);
void putc(c);
void clrscr();
void set_cursor(uint16_t col, uint16_t row);
uint16_t getc();
int _kbhit();
void simple_delay(uint16_t del);
void draw_paddle(int x, int y, int a);
void draw_ball(int x, int y, char c);

int main() {
    uint16_t c;
    uint8_t b;

    int ball_x, ball_y;
    int dir_x, dir_y;
    int paddle_l_y, paddle_r_y;

    ball_x = 80/2;
    ball_y = 25/2;
    dir_x = 1;
    dir_y = 1;
    paddle_l_y = (25-10)/2;
    paddle_r_y = (25-10)/2;

	#asm
		mov ah, #0x01
		mov cx, #0x2607		// invisible cursor
		int 0x10
	#endasm

    for(;;) {
        draw_paddle(1, paddle_l_y, 1);
        draw_paddle(78, paddle_r_y, 1);

        if(dir_x) {
            draw_ball(ball_x, ball_y, ' ');
            ball_x++;
            if(ball_x > 78) {   //hit right
                ball_x = 78;
                dir_x = 0;
            }
        } else {
            draw_ball(ball_x, ball_y, ' ');
            ball_x--;
            if(ball_x < 2) {    // hit left
                ball_x = 2;
                dir_x = 1;
            }
        }
        if(dir_y) {
            draw_ball(ball_x, ball_y, ' ');
            ball_y++;
            if(ball_y > 24) {   // hit bottom
                ball_y = 24;
                dir_y = 0;
            }
        } else {
            draw_ball(ball_x, ball_y, ' ');
            ball_y--;
            if(ball_y < 2) {    // hit top
                ball_y = 2;
                dir_y = 1;
            }
        }
        draw_ball(ball_x, ball_y, 0x0F);
        simple_delay(1);

        c = _kbhit();
        if(c != 0x00) {
            switch(c) {
                case 0x011b:    // esc
                    #asm
                        mov ah, 0x4c
                        int 0x21
                    #endasm                    
                    break;
                case 0x4800:	// R UP, arrow up
                    draw_paddle(78, paddle_r_y, 0);
                    paddle_r_y--;
                    if(paddle_r_y < 1) paddle_r_y = 1;
                    break;
                case 0x5000:	// R DOWN, arrow down
                    draw_paddle(78, paddle_r_y, 0);
                    paddle_r_y++;
                    if(paddle_r_y > 14) paddle_r_y = 14;
                    break;
                case 0x1071:	// L UP, q
                    draw_paddle(1, paddle_l_y, 0);
                    paddle_l_y--;
                    if(paddle_l_y < 1) paddle_l_y = 1;
                    break;
                case 0x1e61:	// L DOWN, a
                    draw_paddle(1, paddle_l_y, 0);
                    paddle_l_y++;
                    if(paddle_l_y > 14) paddle_l_y = 14;
                    break;                
            }
        }
    }
    
}

void draw_ball(int x, int y, char c) {
    set_cursor(x, y);
    putc(c);
}

void draw_paddle(int x, int y, int a) {
    int _y;

    for(_y = 0; _y < 10; _y++) {
        set_cursor(x, _y+y);
        if(a) putc(0xDB);
        else putc(' ');
    }
    
}

void simple_delay(uint16_t del) {
	#asm
		mov bx, sp
		mov di, [bx+2]
		mov ah, #0x00
		int 0x1A		; get time
		mov bx,dx
		del_loop:
                mov ah, #0x00
                int #0x1A
                sub dx,bx
                cmp di,dx
		ja del_loop
    #endasm
}

/* returns:
 *  no keystroke = 0x00
 *  keystroke    = returns AX => AH = SCANCODE, AL=ASCII */
int _kbhit() {
    #asm
        mov ah, #0x01
        int 0x16
        jz nothing
        xor ax, ax
        int 0x16
        jmp outhere
nothing:
        xor ax, ax
outhere:
    #endasm
}

void putc(c) {
	#asm
		mov ah, #0x0E	; int function Eh, write char, move cursor
		mov bx, sp
		mov al, [bx+2]	; arguments are on stack, 
                        ; see cdecl calling conventions
		mov bh, #0	    ; page
		mov cx, #1      ;number of chars
		int 0x10
	#endasm
}

void set_cursor(uint16_t col, uint16_t row) {
	#asm
		mov bx, sp
		mov dh, [bx+4]
		mov dl, [bx+2]
		xor bx, bx        ; page 0
		mov ah, #0x02     ; DH = row, DL = col
		int 0x10
	#endasm
}
