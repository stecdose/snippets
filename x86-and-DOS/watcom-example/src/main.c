#include <stdio.h>

/* prototype for subfunction in test.c */
extern void function_external(void);

int main(int argc, char **argv) {
	printf("hello world\n");
	function_external();
	return 0;
}

