#include <X11/Xlib.h>

int x11_screenres(int *x, int *y) {
	Display* pdsp = XOpenDisplay(NULL);
	Window wid = DefaultRootWindow(pdsp);
	
	XWindowAttributes xwAttr;
	XGetWindowAttributes(pdsp,wid,&xwAttr);

	*x = xwAttr.width;
	*y = xwAttr.height;

	XCloseDisplay(pdsp);

	return 0;
}

