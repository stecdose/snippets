#ifndef __6502_H__
#define __6502_H__

#include <stdint.h>
#include <inttypes.h>

#define PIXEL_SIZE_X	20
#define PIXEL_SIZE_Y	20

#define A	register_a
#define X	register_x
#define Y	register_y
#define P	register_flag
#define S	register_stackptr
#define PC	register_pc

#define ZP_START	0x0000
#define ZP_END		0x00FF
#define ZP_LEN		256

#define STACK_START	0x0100
#define STACK_END	0x01FF
#define STACK_LEN	256

#define RANDOM_OFFSET	0xFE
#define RANDOM_ADDR	(ZP_START+RANDOM_OFFSET)
#define RNG_ADDR	RANDOM_ADDR

#define INPUT_OFFSET	0xFF
#define INPUT_ADDR	(ZP_START+ASCII_OFFSET)

#define OUTPUT_OFFSET	0xFD
#define OUTPUT_ADDR	(ZP_START+OUTPUT_OFFSET)

#define GRAPHICS_ADDR	0x200
#define GRAPHICS_SIZE	1024



extern uint32_t ticks[256];
extern int stop_emu();
extern void init6502(void);
extern int init_graphics();
extern int stop_graphics_thread();
extern int is_graphics_thread_running();
extern uint8_t get_mem(uint16_t addr);
extern void put_mem(uint16_t addr, uint8_t data);

extern void reset6502(void);

extern void nmi6502();
extern void irq6502();

extern uint16_t nmi_addr();
extern uint16_t irq_addr();
extern uint16_t rst_addr();

extern uint8_t register_a;
extern uint8_t register_x;
extern uint8_t register_y;
extern uint8_t register_flag;
extern uint8_t register_stackptr;
extern uint16_t register_pc;

extern uint8_t opcode;
extern void (*instruction[256])();

#endif /* __6502_H__ */

