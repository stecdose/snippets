#ifndef __C64_COLORS__
#define __C64_COLORS__

/* we want ncurses and sdl color definitions.
 *
 * SDL color definitions are 4 bytes in length, RGBA values are taken.
 * ncurses makes use of RGB val
 */

/* ncurses lib takes RGB values from 0 to 1000, 8bit color values 
 * are written to the comments. 
 * 8bit rgb color values will result in a color depth of 24bit. For example 
 * SDL uses a 32bit color datatype: RGBA each of 8bit. A is Alpha and should 
 * be set to 0xff normally. 
 * simple "shading" could be achieved by decreasing this value.
 */

#define C64_COLORS_0_BLACK_R	0x000
#define C64_COLORS_0_BLACK_G	0x000
#define C64_COLORS_0_BLACK_B	0x000

#define C64_COLORS_1_WHITE_R	0x3f8
#define C64_COLORS_1_WHITE_G	0x3f8
#define C64_COLORS_1_WHITE_B	0x3f8

#define C64_COLORS_2_RED_R    	0x3f8
#define C64_COLORS_2_RED_G    	0x000
#define C64_COLORS_2_RED_B    	0x000

#define C64_COLORS_3_CYAN_R    	0x08c
#define C64_COLORS_3_CYAN_G    	0x2a8
#define C64_COLORS_3_CYAN_B    	0x3f8

#define C64_COLORS_4_PURPLE_R   0x20d
#define C64_COLORS_4_PURPLE_G   0x111
#define C64_COLORS_4_PURPLE_B   0x20d

#define C64_COLORS_5_GREEN_R    0x000
#define C64_COLORS_5_GREEN_G    0x3f8
#define C64_COLORS_5_GREEN_B    0x000

#define C64_COLORS_6_BLUE_R    	0x000
#define C64_COLORS_6_BLUE_G    	0x000
#define C64_COLORS_6_BLUE_B    	0x3f8

#define C64_COLORS_7_YELLOW_R   0x382
#define C64_COLORS_7_YELLOW_G   0x35b
#define C64_COLORS_7_YELLOW_B   0x000

#define C64_COLORS_8_ORANGE_R   0x382
#define C64_COLORS_8_ORANGE_G   0x25d
#define C64_COLORS_8_ORANGE_B   0x14c

#define C64_COLORS_9_BROWN_R    0x187
#define C64_COLORS_9_BROWN_G    0x111
#define C64_COLORS_9_BROWN_B    0x000

#define C64_COLORS_10_LRED_R    0x3f8
#define C64_COLORS_10_LRED_G    0x1d5
#define C64_COLORS_10_LRED_B    0x1d5

#define C64_COLORS_11_DGRAY_R   0x0c3
#define C64_COLORS_11_DGRAY_G   0x0c3
#define C64_COLORS_11_DGRAY_B   0x0c3

#define C64_COLORS_12_GRAY_R    0x187
#define C64_COLORS_12_GRAY_G    0x187
#define C64_COLORS_12_GRAY_B    0x187

#define C64_COLORS_13_LGREEN_R  0x298
#define C64_COLORS_13_LGREEN_G  0x3f8
#define C64_COLORS_13_LGREEN_B  0x187

#define C64_COLORS_14_LBLUE_R   0x027
#define C64_COLORS_14_LBLUE_G   0x2f1
#define C64_COLORS_14_LBLUE_B   0x3aa

#define C64_COLORS_15_LGREY_R   0x2bf
#define C64_COLORS_15_LGREY_G   0x2bf
#define C64_COLORS_15_LGREY_B   0x2bf


/* now all colors again for SDL */

/* these are the color definitions of the c64 color palette for use
 * with SDL. They are in 8bit RGBA-format. */

#define sdl_color0r 0x00
#define sdl_color0g 0x00
#define sdl_color0b 0x00
#define sdl_color0a 0xff

#define sdl_color1r 0xff
#define sdl_color1g 0xff
#define sdl_color1b 0xff
#define sdl_color1a 0xff

#define sdl_color2r 0xff
#define sdl_color2g 0x00
#define sdl_color2b 0x00
#define sdl_color2a 0xff

#define sdl_color3r 0x28
#define sdl_color3g 0xf0
#define sdl_color3b 0xff
#define sdl_color3a 0xff

#define sdl_color4r 0xc8
#define sdl_color4g 0x46
#define sdl_color4b 0xc8
#define sdl_color4a 0xff

#define sdl_color5r 0x00
#define sdl_color5g 0xff
#define sdl_color5b 0x00
#define sdl_color5a 0xff

#define sdl_color6r 0x00
#define sdl_color6g 0x00
#define sdl_color6b 0xff
#define sdl_color6a 0xff

#define sdl_color7r 0xe6
#define sdl_color7g 0xdc
#define sdl_color7b 0x00
#define sdl_color7a 0xff

#define sdl_color8r 0xe6
#define sdl_color8g 0x9b
#define sdl_color8b 0x55
#define sdl_color8a 0xff

#define sdl_color9r 0x64
#define sdl_color9g 0x46
#define sdl_color9b 0x00
#define sdl_color9a 0xff 

#define sdl_colorAr 0xff
#define sdl_colorAg 0x78
#define sdl_colorAb 0x78
#define sdl_colorAa 0xff

#define sdl_colorBr 0x32
#define sdl_colorBg 0x32
#define sdl_colorBb 0x32
#define sdl_colorBa 0xff

#define sdl_colorCr 0x64
#define sdl_colorCg 0x64
#define sdl_colorCb 0x64
#define sdl_colorCa 0xff

#define sdl_colorDr 0xaa
#define sdl_colorDg 0xff
#define sdl_colorDb 0x64
#define sdl_colorDa 0xff

#define sdl_colorEr 0x0a
#define sdl_colorEg 0xa0
#define sdl_colorEb 0xf0
#define sdl_colorEa 0xff

#define sdl_colorFr 0xb4
#define sdl_colorFg 0xb4
#define sdl_colorFb 0xb4
#define sdl_colorFa 0xff



#endif

