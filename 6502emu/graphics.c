#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>

#include "6502.h"
#include "io.h"

SDL_Surface *screen;
static SDL_Thread *t_gfx;
static int graphics_thread_running;

static int set_px(int x, int y, unsigned char pxcol);
static int graphics_thread(void *parameter);

int init_graphics() {	
	if(SDL_Init(SDL_INIT_VIDEO) == -1) {
		printf(A_RED A_BOLD "Can't init SDL:  %s\n" A_RST, SDL_GetError());
		exit(1);
	}

	atexit(SDL_Quit);

	screen = SDL_SetVideoMode(32*PIXEL_SIZE_X, 32*PIXEL_SIZE_Y, 16, SDL_HWSURFACE);

	if(screen == NULL) {
		printf(A_RED A_BOLD "Can't set SDL video mode: %s\n" A_RST, SDL_GetError());
		exit(1);
	}

	printf(A_YELLOW "init_graphics(): "A_BOLD"SDL_Init()+SDL_SetVideoMode"A_RST A_GREEN " OK" A_BOLD " OK\n" A_RST);
	printf(A_YELLOW "screen resolution " A_BOLD " %dx%d ", 32*PIXEL_SIZE_X, 32*PIXEL_SIZE_Y);

	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
	boxRGBA(screen, 0, 0, 32*8, 32*8, 0, 0, 0, 255);
	
	SDL_WM_SetCaption("6502asm.com emulator", NULL);

	graphics_thread_running = 1;


	t_gfx = SDL_CreateThread(graphics_thread, NULL);

	printf(A_BOLD "init_graphics(): thread created....\n");

	printf(A_CYAN "init_graphics(): will throw more info at a later stage of development.\n"A_RST);	
	
	return 0;
}

static int graphics_thread(void *parameter) {
	int px_x, px_y;
	uint16_t px_addr;

	while(graphics_thread_running) {
		for(px_y = 0; px_y < 32; px_y++) {
			for(px_x = 0; px_x < 32; px_x++) {
				px_addr = GRAPHICS_ADDR+(px_x+(px_y*32));
				set_px(px_x, px_y, get_mem(px_addr));
			}
		}
		SDL_UpdateRect(screen, 0, 0, 0, 0);
	}
	return 0;
}

int stop_graphics_thread() {
	if(is_graphics_thread_running())
		graphics_thread_running = 0;
	else return -1;
	return 0;
}

int is_graphics_thread_running() {
	return graphics_thread_running;
}

static int set_px(int x, int y, unsigned char pxcol) {
	unsigned char r, g, b, a = 255;

	pxcol &= 0x0f;

	switch(pxcol) {
		case 0x00: 
			r = 0; 		// black
			g = 0; 
			b = 0;
			break;
		case 0x01: 
			r = 255;	// white 
			b = 255; 
			g = 255;
			break;
		case 0x02:		// red
			r = 135;
			g = 0;
			b = 0;
			break;
		case 0x03:		// cyan
			r = 40;
			g = 240;
			b = 255;
			break;
		case 0x04:		// purple
			r = 200;
			g = 70;
			b = 200;
			break;
		case 0x05:		// green
			r = 0;
			g = 255;
			b = 0;
			break;
		case 0x06:		// blue
			r = 0;
			g = 0;
			b = 255;
			break;
		case 0x07:		// yellow
			r = 230;
			g = 220;
			b = 10;
			break;
		case 0x08:		// orange
			r = 230;
			g = 155;
			b = 85;
			break;
		case 0x09:		// brown
			r = 100;
			g = 70;
			b = 0;
			break;
		case 0x0A:		// light red
			r = 255;
			g = 120;
			b = 120;
			break;
		case 0x0B:		// dark gray
			r = 50;
			g = 50;
			b = 50;
			break;
		case 0x0C:		// gray
			r = 100;
			g = 100;
			b = 100;
			break;
		case 0x0D:		// light green
			r = 170;
			g = 255;
			b = 100;
			break;
		case 0x0E:		// light blue
			r = 10;
			g = 160;
			b = 240;
			break;
		case 0x0F:		// light gray
			r = 180;
			g = 180;
			b = 180;
			break;
	}
	boxRGBA(screen, x*PIXEL_SIZE_X, y*PIXEL_SIZE_Y, (x*PIXEL_SIZE_X)+PIXEL_SIZE_X, (y*PIXEL_SIZE_Y)+PIXEL_SIZE_Y, r, g, b, a);
	
	return 0;
}

