#ifndef __IO_H__
#define __IO_H__

#include <inttypes.h>

#define A_RST	  "\033[0m"
#define A_BOLD	  "\033[1m"
#define A_BLACK   "\033[30m"
#define A_RED     "\033[31m"
#define A_GREEN   "\033[32m"
#define A_YELLOW  "\033[33m"
#define A_BLUE    "\033[34m"
#define A_MAGENTA "\033[35m"
#define A_CYAN    "\033[36m"
#define A_WHITE   "\033[37m"

int32_t load_rom(char *filename, uint16_t offset);
void mem_clear();
#endif // __IO_H__
