#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include "6502.h"
#include "io.h"

extern SDL_Surface *screen;

/* debugger related functions, globals and definitions */
void print_state();
uint32_t step;
uint32_t clockticks;

/* io related */
extern uint8_t get_mem(uint16_t addr);
extern void put_mem(uint16_t addr, uint8_t data);

static int cpu_thread_running;
static SDL_Thread *t_cpu;

uint8_t system_memory[0x10000];


int main(int argc, char **argv) {
	printf(A_MAGENTA A_BOLD "6502asm.com emulator\n" A_RST A_MAGENTA \
	       "  (c) 2007-2017 Nils Stec and others, see documentation.\n"\
	       "  compile time %s - %s\n" A_RST, __DATE__, __TIME__);
	printf("  this emulator is fully compatible to the hypothetic machine as\n");
	printf("  seen on " A_CYAN A_BOLD "http://6502asm.com"A_RST", please also visit:\n");
	printf(A_CYAN A_BOLD"  http://spacerace.github.com\n"A_RST);

        if(argc < 4) { 
		printf(A_BOLD A_RED "arg error.\n" A_RST); 
		printf("argument 0 = %s\n", argv[0]);
		printf("argument 1 = rom file\n");
		printf("argument 2 = rom load address int hex\n");
		printf("argument 3 = initial PC\n");
		
		exit(-1); 
	}
	
	init_graphics();
	mem_clear();
	init6502();
	
	printf(A_RST "init6502(): mem clear, set initial values," A_GREEN " done.\n" A_RST);
	
	reset6502();
	uint16_t load_addr = (uint16_t)strtol(argv[2], NULL, 16);

	if(load_rom(argv[1], load_addr)==-1) {
		printf("failed when trying to load ROM image ... \n");
		exit(-1);
	}

	PC = (uint16_t)strtol(argv[3], NULL, 16);	
	printf("initial PC="A_CYAN"$%04x\n"A_RST, PC);

	srand(time(NULL));
	printf("RNG ok.\n");
	
	int done = 0;
	uint8_t *keys;
	//uint32_t intcount = 0;
	//uint32_t nmicount = 0;
	//uint32_t resetcount = 0;

	step = 0;

	SDL_Event event;
	opcode = system_memory[PC+1];
	print_state();

	while(!done) {
		while(SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_QUIT:
					done = 1;
					break;
			}
		}
		keys = SDL_GetKeyState(NULL);
		if(keys[SDLK_ESCAPE]) {
			stop_emu();
			printf(A_RED "esc -> done.\n" A_RST);
			done = 1;
		}
	}

	SDL_Quit();
	printf("we're at the end...\n");
	return 0;
}

/*    // fetch instruction
      opcode = gameImage[PC++];

      // execute instruction
      instruction[opcode]();

      // calculate clock cycles
      clockticks6502 += ticks[opcode];
      timerTicks -= clockticks6502;
      clockticks6502 = 0;
      */

static int cpu_thread(void *parameter) {
	cpu_thread_running = 1;

	while(cpu_thread_running) {
		opcode = system_memory[PC++];
		instruction[opcode]();
	}

	return 0;
}

int is_cpu_thread_running() {
	return cpu_thread_running;
}

int stop_cpu_thread() {
	if(cpu_thread_running)
		cpu_thread_running = 0;
	else return -1;
	return 0;
}

int init_cpu_thread() {
	cpu_thread_running = 1;
	t_cpu = SDL_CreateThread(cpu_thread, NULL);
	
	return 0;
}

void print_state() {
	printf("step %012d | ", step);
	printf("A="A_CYAN"$%02x"A_RST" X="A_CYAN"$%02x"A_RST" Y="A_CYAN"$%02x"A_RST" ", A, X, Y);
	printf("P="A_CYAN"$%02x "A_RST, P);

	printf("[");
	if(P & 0x80) printf(A_CYAN "N" A_RST);
	else         printf("n");
	if(P & 0x40) printf(A_CYAN "V" A_RST);
	else         printf("v");
	if(P & 0x20) printf("-");
	else         printf(A_CYAN "_" A_RST);
	if(P & 0x10) printf(A_CYAN "B" A_RST);
	else         printf("b");
	if(P & 0x08) printf(A_CYAN "D" A_RST);
	else         printf("d");
	if(P & 0x04) printf(A_CYAN "I" A_RST);
	else         printf("i");
	if(P & 0x02) printf(A_CYAN "Z" A_RST);
	else         printf("z");
	if(P & 0x01) printf(A_CYAN "C" A_RST);
	else         printf("c");
	printf("] ");

	printf("S="A_CYAN"$%02x "A_RST, S);
	printf("PC="A_CYAN"$%04x "A_RST, PC);
	printf("| OPCODE=$%02x\n", opcode);
	return;
}


int stop_emu() {
	printf(A_BOLD A_YELLOW "stop_emu(): shutting down all threads...\n");
	stop_graphics_thread();
	printf("waiting for graphics_thread() to terminate ");
	while(is_graphics_thread_running()) {
		printf(".");
	}
	stop_cpu_thread();
	printf("\nwaiting for cpu_thread() to terminate ");
	while(is_cpu_thread_running()) {
		printf(".");
	}
		
	printf(A_RST"\n");
	return 0;
}


#define FORMAT_BIN	0x01
#define FORMAT_ASCII    0x02

/* file format:
 *
 * <header>     
 * <registers>
 * <debugger information>
 * <
 *
 *
 * */


