#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>



#include "io.h"

extern uint8_t system_memory[0x10000];	/* 64k system memory */

void mem_clear() {
	int i;
	for(i = 0; i <= 0xffff; i++) system_memory[i] = 0;
	return;
}

int32_t load_rom(char *filename, uint16_t offset) {
	uint32_t addr;
	uint8_t data;
	FILE *f = fopen(filename, "r");

	if(f == NULL) {
		printf(A_RED A_BOLD "load_rom(): can't open image!\n" A_RST);
		return -1;
	}

	struct stat stbuf;

	stat(filename, &stbuf);

	if((stbuf.st_size + offset) > 0x10000)
		printf(A_RED "load_rom(): warning, image exceeds memory, loading anyway...\n"A_RST);

	printf("load_rom(): file %s, size $%04x bytes, load addr $%04x, ", filename, (uint32_t)stbuf.st_size, offset);

	addr = offset;
	while(addr <= 0xffff) {
		data = getc(f);
		system_memory[addr] = data;
		addr++;
	}

	fclose(f);

	printf(A_GREEN" done.\n"A_RST);

	return 0;

}


