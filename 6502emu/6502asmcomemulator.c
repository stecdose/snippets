/* 6502 emulator */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include "6502.h"

extern SDL_Surface *screen;

/* memory related */
extern int32_t load_rom(char *filename, uint16_t offset);
extern void mem_clear();

/* debugger related functions, globals and definitions */
extern void print_state();
extern uint32_t step;
extern uint32_t clockticks;

/* io related */
extern uint8_t get_mem(uint16_t addr);

int go_to_emu() {
	SDL_Quit();
	printf("we're at the end...\n");
	exit(0);
}
