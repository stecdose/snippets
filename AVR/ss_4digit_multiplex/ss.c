#include "ss.h"
#include "LED.h"
#include "4094.h"
#include "74164.h"
#include <stdio.h>
#include <stdlib.h>

const uint8_t hex_charset[16] = { NUM_0, 
							NUM_1, 
							NUM_2, 
							NUM_3, 
							NUM_4, 
							NUM_5, 
							NUM_6, 
							NUM_7, 
							NUM_8, 
							NUM_9, 
							NUM_A, 
							NUM_B, 
							NUM_C, 
							NUM_D, 
							NUM_E, 
							NUM_F, 
};

int init_display_timer(void) {    
    TCCR0= (1<<CS01)|(1<<CS00);       // Start Timer 0 with prescaler 1024
    TIMSK= (1<<TOIE0);                // Enable Timer 0 overflow interrupt
	return 0;
}

void init_mpx_digits(void) {
    DDRB |= (1<<PB3);	// D2
	DDRB |= (1<<PB4);	// D3
	DDRB |= (1<<PB5);	// D4
	DDRC |= (1<<PC4);	// D1
	MPX_ALL_OFF();	                               //  
 
}



ISR(TIMER0_OVF_vect) {
	static uint16_t ticks = 0;
	ticks++;
  	TCNT0 = 5; 
	
	if((ticks%20)==0){
		sr4094_sendbyte(~0x01);
		sr4094_sendbyte(~0x02);
		sr4094_sendbyte(~0x04);

	}                                    // f_isr = 16MHz/64/250 = 1kHz
	if((ticks%1000)==0){
 		LED_ORANGE_TOGGLE(); 
 		ticks =0;
 	}
	return 0;
} 

