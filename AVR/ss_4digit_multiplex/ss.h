#ifndef __SS_H__
#define __SS_H__
#include <avr/io.h>
#include <util/delay.h>

#define SEG_A 0x01
#define SEG_B 0x02
#define SEG_C 0x04
#define SEG_D 0x08
#define SEG_E 0x10
#define SEG_F 0x20
#define SEG_G 0x40
#define SEG_dp 0x80

#define NUM_0   SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F
#define NUM_1   SEG_B|SEG_C
#define NUM_2   SEG_A|SEG_B|SEG_D|SEG_E|SEG_G
#define NUM_3   SEG_A|SEG_B|SEG_C|SEG_D|SEG_G
#define NUM_4   SEG_B|SEG_C|SEG_F|SEG_G
#define NUM_5   SEG_A|SEG_F|SEG_G|SEG_C|SEG_D
#define NUM_6   SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_7   SEG_A|SEG_B|SEG_C
#define NUM_8   SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_9   SEG_A|SEG_B|SEG_C|SEG_D|SEG_F|SEG_G
#define NUM_A   SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G
#define NUM_B   SEG_C|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_C   SEG_A|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_D   SEG_A|SEG_B|SEG_C|SEG_D|SEG_E
#define NUM_E   SEG_A|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_F   SEG_A|SEG_B|SEG_E|SEG_F|SEG_G

#define SEG_A_4094 0x02
#define SEG_B_4094 0x04
#define SEG_C_4094 0x08
#define SEG_D_4094 0x10
#define SEG_E_4094 0x80
#define SEG_F_4094 0x40
#define SEG_G_4094 0x20
#define SEG_dp_4094 0x01


#define MPX_ALL_OFF()	PORTB |= (1<<PB3)|(1<<PB4)|(1<<PB5); PORTC |= (1<<PC4);	// all digits off
#define MPX_DIG1_ON()	PORTC &= ~(1<<PC4);	// D1
#define MPX_DIG2_ON()   PORTB &= ~(1<<PB3);	// D2
#define MPX_DIG3_ON()	PORTB &= ~(1<<PB4);	// D3
#define MPX_DIG4_ON()	PORTB &= ~(1<<PB5);	// D4


void init_mpx_digits(void);
int init_display_timer(void);
#endif
