#include "74164.h"
#include "util.h"

void init_sr74164(void) {
   	DDRD |=	(1<<PD7);	// CLK
	PORTD |= (1<<PD7);	// CLK

	DDRB |= (1<<PB0);	// DATA
	PORTB &= ~(1<<PB0);	// DATA


	sr74164_sendbyte(~0x40);
}

void sr74164_sendbyte(uint8_t data) {
	uint8_t i;
 
	for(i=0x80; i>=1; i/=2) {

		if(data & i)
			PORTB |= (1<<PB0);
		else
			PORTB &= ~(1<<PB0);
        /* CLK */
		
		PORTD &= ~(1<<PD7);
		PORTD |= (1<<PD7);
		
    }
}

