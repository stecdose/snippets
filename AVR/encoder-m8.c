/*
 *
 *    example for AVR ATMega8(L)
 *
 * UART code comes from mikrocontroller.net wiki
 *
 */

#ifndef F_CPU
#define F_CPU 10000000	// my devboard uses a 10MHz XTAL
#endif

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "i2cmaster.h"
#include "pcf8576.h"
#include "scf5740.h"

#define BAR0_BYTE	28
#define BAR0_BIT	5
#define BAR1_BYTE	28
#define BAR1_BIT	6
#define BAR2_BYTE	24
#define BAR2_BIT	6
#define BAR3_BYTE	24
#define BAR3_BIT	5
#define BAR4_BYTE	24
#define BAR4_BIT	7
#define BAR5_BYTE	20
#define BAR5_BIT	7
#define BAR6_BYTE	20
#define BAR6_BIT	5
#define BAR7_BYTE	20
#define BAR7_BIT	6
#define BAR8_BYTE	16
#define BAR8_BIT	6
#define BAR9_BYTE	16
#define BAR9_BIT	5
#define BAR10_BYTE	16
#define BAR10_BIT	7
#define BAR11_BYTE	12
#define BAR11_BIT	7
#define BAR12_BYTE	12
#define BAR12_BIT	5
#define BAR13_BYTE	12
#define BAR13_BIT	6
#define BAR14_BYTE	8
#define BAR14_BIT	6
#define BAR15_BYTE	8
#define BAR15_BIT	5

#define SYMBOL_BAR0		((BAR0_BYTE<<3)|BAR0_BIT)
#define SYMBOL_BAR1		((BAR1_BYTE<<3)|BAR1_BIT)
#define SYMBOL_BAR2		((BAR2_BYTE<<3)|BAR2_BIT)
#define SYMBOL_BAR3		((BAR3_BYTE<<3)|BAR3_BIT)
#define SYMBOL_BAR4		((BAR4_BYTE<<3)|BAR4_BIT)
#define SYMBOL_BAR5		((BAR5_BYTE<<3)|BAR5_BIT)
#define SYMBOL_BAR6		((BAR6_BYTE<<3)|BAR6_BIT)
#define SYMBOL_BAR7		((BAR7_BYTE<<3)|BAR7_BIT)
#define SYMBOL_BAR8		((BAR8_BYTE<<3)|BAR8_BIT)
#define SYMBOL_BAR9		((BAR9_BYTE<<3)|BAR9_BIT)
#define SYMBOL_BAR10	((BAR10_BYTE<<3)|BAR10_BIT)
#define SYMBOL_BAR11	((BAR11_BYTE<<3)|BAR11_BIT)
#define SYMBOL_BAR12	((BAR12_BYTE<<3)|BAR12_BIT)
#define SYMBOL_BAR13	((BAR13_BYTE<<3)|BAR13_BIT)
#define SYMBOL_BAR14	((BAR14_BYTE<<3)|BAR14_BIT)
#define SYMBOL_BAR15	((BAR15_BYTE<<3)|BAR15_BIT)

#define SYMBOL_FREQUENCY	((38<<3)|1)
#define SYMBOL_PERIOD		((35<<3)|5)
#define SYMBOL_RATIO		((31<<3)|5)
#define SYMBOL_INTERVAL		((27<<3)|5)
#define SYMBOL_AVERAGE		((23<<3)|5)
#define SYMBOL_PRESCALE		((19<<3)|5)
#define SYMBOL_OVERRANGE	((15<<3)|5)
#define SYMBOL_A			((11<<3)|5)
#define SYMBOL_B			((7<<3)|5)
#define SYMBOL_HZ			((3<<3)|5)
#define SYMBOL_MHZ			((4<<3)|7)
#define SYMBOL_S			((4<<3)|5)
#define SYMBOL_US			((4<<3)|6)
#define SYMBOL_NS			((8<<3)|7)
#define SYMBOL_0_01			((36<<3)|7)
#define SYMBOL_0_1			((36<<3)|5)
#define SYMBOL_1_0			((36<<3)|6)
#define SYMBOL_10			((32<<3)|6)
#define SYMBOL_100			((32<<3)|5)
#define SYMBOL_1000			((32<<3)|7)
#define SYMBOL_LOWBATT		((28<<3)|7)

static const uint16_t symbols[21] PROGMEM = { 
	(uint16_t)SYMBOL_FREQUENCY,     // 0 OK
	(uint16_t)SYMBOL_PERIOD, 	    // 1 OK
	(uint16_t)SYMBOL_RATIO, 		// 2 OK
	(uint16_t)SYMBOL_INTERVAL, 	    // 3 OK
	(uint16_t)SYMBOL_AVERAGE,	    // 4 OK
	(uint16_t)SYMBOL_PRESCALE, 	    // 5 OK
	(uint16_t)SYMBOL_OVERRANGE, 	// 6 OK
	(uint16_t)SYMBOL_A, 			// 7 OK
	(uint16_t)SYMBOL_B, 			// 8 OK
	(uint16_t)SYMBOL_HZ, 		    // 9 OK
	(uint16_t)SYMBOL_MHZ,		    // 10 OK
	(uint16_t)SYMBOL_S, 			// 11 OK
	(uint16_t)SYMBOL_US, 		    // 12 OK
	(uint16_t)SYMBOL_NS, 		    // 13 OK
	(uint16_t)SYMBOL_0_01, 		    // 14
	(uint16_t)SYMBOL_0_1, 		    // 15
	(uint16_t)SYMBOL_1_0, 		    // 16
	(uint16_t)SYMBOL_10, 		    // 17
	(uint16_t)SYMBOL_100,		    // 18
	(uint16_t)SYMBOL_1000, 		    // 19
	(uint16_t)SYMBOL_LOWBATT		// 20
};

static const uint16_t symbols_seven_segments[7] = {
	0,	//	A
	0,	//  B
	0,	//  C
	0,	//  D
	0,	//  E
	0,	//  F
	0,	//  G
};

static const uint8_t symbols_bar[16] PROGMEM = { 
	(uint8_t)SYMBOL_BAR0,
	(uint8_t)SYMBOL_BAR1, 
	(uint8_t)SYMBOL_BAR2, 
	(uint8_t)SYMBOL_BAR3,
	(uint8_t)SYMBOL_BAR4, 
	(uint8_t)SYMBOL_BAR5, 
	(uint8_t)SYMBOL_BAR6, 
	(uint8_t)SYMBOL_BAR7,
	(uint8_t)SYMBOL_BAR8, 
	(uint8_t)SYMBOL_BAR9, 
	(uint8_t)SYMBOL_BAR10, 
	(uint8_t)SYMBOL_BAR11,
	(uint8_t)SYMBOL_BAR12, 
	(uint8_t)SYMBOL_BAR13, 
	(uint8_t)SYMBOL_BAR14, 
	(uint8_t)SYMBOL_BAR15,
};

void pcf8576_set_bar(uint8_t level) {
	uint8_t bit = pgm_read_byte(&symbols_bar[level]);
	uint8_t byte = bit;
	
	bit &= 0x07;
	byte = (byte >> 3);
	
	pcf8576_set_ram(byte, (1<<bit));
}

void pcf8576_symbol(uint8_t symbol) {	
	uint16_t addr = pgm_read_word(&symbols[symbol]);
	uint8_t bit = (uint8_t)(addr&0x07);
	uint8_t byte = (uint8_t)(addr>>3);
	pcf8576_set_ram(byte, (1<<bit));
}

#define PORT_DATA	PORTC
#define DDR_DATA	DDRC
#define PIN_DATA	PC2

#define PORT_SRCK	PORTC
#define DDR_SRCK	DDRC
#define PIN_SRCK	PC3

#define PORT_RCK	PORTD
#define DDR_RCK		DDRD
#define PIN_RCK		PD3

#define PORT_G		PORTD
#define DDR_G		DDRD
#define PIN_G		PD6

#define SR_OUTPUT_ENA()	PORT_G &= ~(1<<PIN_G)
#define SR_OUTPUT_DIS()	PORT_G |= (1<<PIN_G)

#define RCK_SET()	PORT_RCK |= (1<<PIN_RCK)
#define RCK_CLR()	PORT_RCK &= ~(1<<PIN_RCK)
#define SRCK_SET()	PORT_SRCK |= (1<<PIN_SRCK)
#define SRCK_CLR()	PORT_SRCK &= ~(1<<PIN_SRCK)
#define DATA_SET()	PORT_DATA |= (1<<PIN_DATA)
#define DATA_CLR()	PORT_DATA &= ~(1<<PIN_DATA)

void stpic6c595_tx_byte(uint8_t byte) {
	uint8_t i;

	RCK_CLR();
	SRCK_CLR();

	for(i = 0; i <= 7; i++) {
		if(byte & 0x80) {
			PORT_DATA |= (1<<PIN_DATA);
		} else {
			PORT_DATA &= ~(1<<PIN_DATA);
		}
		byte = (uint8_t)(byte << 1);
		
		PORT_SRCK |= (1<<PIN_SRCK);
		PORT_SRCK &= ~(1<<PIN_SRCK);
	}

	PORT_RCK |= (1<<PIN_RCK);
	PORT_RCK &= ~(1<<PIN_RCK);
}

void init_stpic6c595(void) {
	DDR_DATA |= (1<<PIN_DATA);	// DATA
	DDR_SRCK |= (1<<PIN_SRCK);	// SRCK
	DDR_RCK  |= (1<<PIN_RCK);	// RCK
	DDR_G    |= (1<<PIN_G);		// G
	
	RCK_CLR();
	SRCK_CLR();
	
	SR_OUTPUT_ENA();	
}


#define PHASE_A     (PINC & 1<<PC1)
#define PHASE_B     (PINC & 1<<PC0)
volatile int8_t enc_delta;          // -128 ... 127
static int8_t last;
void encode_init( void )
{
  int8_t new;

  new = 0;
  if( PHASE_A ) new = 3;
  if( PHASE_B ) new ^= 1;       // convert gray to binary
  last = new;                   // power on state
  enc_delta = 0;
  //TCCR0 = (1<<WGM01) | (1<<CS01) | (1<<CS00);   // CTC, prescaler 64
  //OCR0 = (uint8_t)(XTAL / 64.0 * 1e-3 - 0.5);   // 1ms
  //TIMSK |= 1<<OCIE0;
}

int8_t encode_read1( void )         // read single step encoders
{
  int8_t val;

  cli();
  val = enc_delta;
  enc_delta = 0;
  sei();
  return val;                   // counts since last call
}

int main(void) {
	int16_t encval = 0;
	//DDRD |= (1<<PD7);
	//init_stpic6c595();
	//stpic6c595_tx_byte(0x00);
	//SR_OUTPUT_DIS();
	_delay_ms(100);
	//SR_OUTPUT_ENA();	
	init_scf5740();
	scf5740_puts_P(PSTR("?\x01"));
	DDRB |= (1<<PB2);
	//PORTB &= ~(1<<PB1);
	PORTB |= (1<<PB2);
    

    encode_init();
    
    TCCR0= (1<<CS01)|(1<<CS00);       // Start Timer 0 with prescaler 1024
    TIMSK= (1<<TOIE0);                // Enable Timer 0 overflow interrupt

	
   sei();                            // Set the I-bit in SREG

	for(;;) {
		encval += encode_read1();
		scf5740_set_cursor(0);
		scf5740_putx16(encval);
		
		scf5740_set_cursor(4);
		scf5740_putx16(encval/4);
		
		//PORTB ^= (1<<PB2);
		_delay_ms(20);
		
	}
	
	//int i;
	//i2c_init();
	//init_pcf8576();
	
	//pcf8576_fill(0);
	//pcf8576_symbol(20);

	for(;;) {
		//for(i = 0; i <= 9; i++) {
			//pcf8576_set_dp(i);
			_delay_ms(500);
		//}

		
	}
}
// Interrupt subroutine for Timer0 Overflow interrupt
ISR(TIMER0_OVF_vect) 
{
    PORTB ^= (1<<PB2);
     int8_t new, diff;

  new = 0;
  if( PHASE_A ) new = 3;
  if( PHASE_B ) new ^= 1;          // convert gray to binary
  diff = last - new;               // difference last - new
  if( diff & 1 ) {                 // bit 0 = value (1)
    last = new;                    // store new as next last
    enc_delta += (diff & 2) - 1;   // bit 1 = direction (+/-)
  }
} 
