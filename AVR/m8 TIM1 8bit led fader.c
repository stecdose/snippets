// m8 TIM1 8bit led fader


#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//#include "uart.h"


/* define CPU frequency in Mhz here if not defined in Makefile */
#ifndef F_CPU
#define F_CPU 4000000UL
#endif

/* 9600 baud */
//#define UART_BAUD_RATE      9600      


void my_delay (uint16_t milliseconds)
{
    for (; milliseconds > 0; milliseconds--)
        _delay_ms (1);
}

int main(void) {
//    uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) ); 
  //  sei();
//    uart_puts("String stored in SRAM\n");
  //  uart_puts_P("String stored in FLASH\n");
    
    DDRB = (1<<PB1) | (1<<PB2);                      // Set Port PB1 and PB2 as Output
    	            
    TCCR1A = (1<<WGM10) | (1<<COM1A1) | (1<<COM1B1);  // Set up the two Control registers of Timer1.
           				              // Wave Form Generation is Fast PWM 8 Bit,
    TCCR1B = (1<<WGM12)     // OC1A and OC1B are cleared on compare match
            |(1<<CS10);               // and set at BOTTOM. Clock Prescaler is off
    OCR1A = 63;                       // Dutycycle of OC1A = 25%
    OCR1B = 127;                      // Dutycycle of OC1B = 50%

	for(;;) {
		pwm_up_down (fading_table8, 32, 50);
		
	}
    return 0;
    
}
