
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "uart.h"

void uart_init (void)
{

                DDRD |= 1 << PIND1;//pin1 of portD as OUTPUT

                DDRD &= ~(1 << PIND0);//pin0 of portD as INPUT

                PORTD |= 1 << PIND0;
    uint16_t ubrr = (uint16_t) ((uint32_t) F_CPU/(16*BAUDRATE) - 1);
 
    UBRRH = (uint8_t) (ubrr>>8);
    UBRRL = (uint8_t) (ubrr);
 
    // UART Receiver und Transmitter anschalten 
    // Data mode 8N1, asynchron 
    UCSRB = (1 << RXEN) | (1 << TXEN);
    UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);

    // Flush Receive-Buffer (entfernen evtl. vorhandener ung�ltiger Werte) 
    do
    {
        UDR;
    }
    while (UCSRA & (1 << RXC));
}

int uart_putc (const uint8_t c)
{
    // Warten, bis UDR bereit ist f�r einen neuen Wert 
    while (!(UCSRA & (1 << UDRE)))
        ;

    // UDR Schreiben startet die �bertragung 
    UDR = c;

    return 1;
}

int uart_puts(char *s) {
	while(*s) {
		uart_putc(*s);
		s++;
	}

	return 0;
}
