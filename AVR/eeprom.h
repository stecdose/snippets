#ifndef __EEPROM_H__
#define __EEPROM_H__

#include <avr/io.h>
#include <util/delay.h>

uint8_t eep2416_write(uint8_t eepaddr, uint16_t addr, uint8_t data);
uint8_t eep2416_read(uint8_t eepaddr, uint16_t addr);

void dumpeep(void);

#endif
