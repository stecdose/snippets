#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/io.h>

#include "random.h"
#include "uart.h"

/* these defines are here to make life easy, rather than handling OCxxx */
#define RED		OCR1B
#define	BLUE	OCR1A
#define GREEN	OCR0A
#define WHITE	OCR0B

/* macros for conveinence */
#define SET_WHITE8(x)	{ WHITE = 255-x; }
#define SET_RED8(x)		{ RED = 4095-(x<<4); }
#define SET_GREEN8(x)	{ GREEN = 255-x; }
#define SET_BLUE8(x)	{ BLUE = 4095-(x<<4); }

int main(void) {
	//uint8_t i;
	UBRRL=12;
	UCSRB = (1<<TXEN);
	UCSRC = (3<<UCSZ0);
	//uart_putc('A');
	//uart_init();
	//uart_puts("RGBW-2313A, v0.00, (c) 2021 stecdose\r\n");
	
	// turn off low side drivers before switching to output, prevents from flashing
	PORTB = (1<<PB2)|(1<<PB3)|(1<<PB4);		
	DDRD = (1<<PD5);   						// white
	DDRB = (1<<PB2)|(1<<PB3)|(1<<PB4);		// green, blue, red

	/* Timer0 PWM - 8bit fast PWM
	 *  OC0A = GREEN, not inverted, because of total off! ( the RGB-LED is of CA type )
	 *  OC0B = WHITE, inverted, because of total off! ( A to uC-pin  )	 */
	TCCR0A = (1<<COM0A1)|(1<<COM0B1)|(1<<COM0B0)|(1<<WGM01)|(1<<WGM00);

	/* Timer1 PWM - mode ??, ??, top=ICR1, 12bit
	 * 	OC1A = BLUE
	 * 	OC1A = RED */
	TCCR1A = (1<<COM1A1)|(1<<COM1B1);
	TCCR1B = (1<<WGM13);
	ICR1 = 4095;

	/* off all */
	SET_WHITE8(0);
	SET_BLUE8(0);
	SET_RED8(0);
	SET_GREEN8(0);

	/* F_PWM(timer0) = 8000000 / 1 / 256 = 31250hz */
    TCCR0B = (1<<CS00);		//psc=1				
    
    /* F_PWM(timer1) = 8000000 / 1 / 256 = 31250hz */
	TCCR1B |= (1<<CS10);	//psc=1

	sei();

	uint8_t phase, step;
	
	phase = 0;	// we only have 6 phases. phase 7 is "start over - reset all"
	step = 0;

	while (1) {
		if((phase+step)==0) {
			RED = 0;
			GREEN = 255;
			BLUE = 4095;
		}

		switch(phase){
			case 0:
				GREEN = GREEN - 1;
				break;
			case 1:
				RED = RED + (1<<4);
				break;
			case 2:
				BLUE = BLUE - (1<<4);
				break;
			case 3:
				GREEN = GREEN + 1;
				break;
			case 4:
				RED = RED - (1<<4);			
				break;
			case 5:
				BLUE = BLUE + (1<<4);			
				break;
		}
		
		if(step < 0xFE) {
			step++;
		} else {
			phase++;
			step = 0;
			
			if(phase == 6)
				phase = 0;
		}
		
		_delay_ms(255);
	}

}

