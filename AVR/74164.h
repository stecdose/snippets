#ifndef __74164_H__
#define __74164_H__
#include <avr/io.h>
#include <util/delay.h>

void init_sr74164(void);
void sr74164_sendbyte(uint8_t data);

#endif
