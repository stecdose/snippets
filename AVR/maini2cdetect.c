/* m18st05b serial VFD
 * 
 *    example for AVR ATMega8(L)
 * 
 * UART code comes from mikrocontroller.net wiki
 * 
 */

#ifndef F_CPU
#define F_CPU 10000000	// my devboard uses a 10MHz XTAL
#endif

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#include "vfd.h"
#include "util.h"
#include "i2cmaster.h"
#include "i2c-hd44780.h"
#include "LED.h"
#include "kb.h"
#include "uart.h"
#include "vfd.h"
#include "scf5740.h"
#include "pcd3311.h"
#include "serial_ss.h"
#include "pcf8576.h"
#include "max7219.h"

int i2cdetect(void) {
	int i;    
    char str[10];

    lcd_puts_P(PSTR("i2c-detect:"));

    sprintf(str, " $%02x = broadcast", 0xff);
    lcd_setcursor(0,1);
    lcd_puts_R(str);
    lcd_setcursor(0,2);
    lcd_putc(' ');
    for(i=0;i<0xff;i+=2){
        if(!i2c_start(i)) {
            i2c_stop();
            sprintf(str, "$%02x,", (uint8_t)i);;
            lcd_puts_R(str);
//             vfd_putx8(i);
//             vfd_putc(',');
            //delay(400);
            if(i==0xa0) i=0xb0;
        }
    }
    return 0;
}

int main(void) {

	i2c_init();
    init_leds();
    lcd_init();
	lcd_clear();
	lcd_setcursor(0, 0);
    
    i2cdetect();

    init_pcf8576();
    pcf8576_fill(0xff);
//     led_fader();
 	
 	pcd3311_phone_number("01782144440", 75);

}
