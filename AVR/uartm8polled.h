#ifndef __UART_H__
#define __UART_H__

/* define CPU frequency in Hz in Makefile */
#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

   
#define BAUDRATE 9600UL //Definition als unsigned long, sonst gibt es Fehler in der Berechnung
void uart_init (void);
int uart_putc (const uint8_t c);
int uart_puts(char *s) ;
#endif
