#include "4094.h"
#include "util.h"

int ani4094 = 0;

int set_anidelay4094(int d) {
	ani4094	= d;
	return d;
}

void init_sr4094(void) {
   	DDRD |=	(1<<PD5);	// CLK
	DDRD |= (1<<PD6);	// DATA
  
	PORTD &= ~(1<<PD6);	// DATA
	PORTD &= ~(1<<PD5);	// CLK

}


void sr4094_sendbyte(uint8_t data) {
	uint8_t i;
	
	for(i=0x80; i>=1; i/=2) {
		if(data & i)
			PORTD |= (1<<PD6);
		else
			PORTD &= ~(1<<PD6);

        /* CLK */
		PORTD |= (1<<PD5);
		PORTD &= ~(1<<PD5);
		if(ani4094)
			_delay(ani4094);
    }
}

