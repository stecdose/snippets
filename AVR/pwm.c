#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "pwm.h"

void init_sw_pwm() {

}

void init_hw_pwm() {
	// TIMER1 ~Hz
	int pwmtop = 4095; 
    OCR1A = pwmtop;
	OCR1B = pwmtop;
    TCCR1A |= (1 << COM1A1)|(1 << COM1B1)|(1<<COM1A0)|(1<<COM1B0);	// inverting
    TCCR1A |= (1 << WGM11)  ;	// 14
    TCCR1B |= (1 << CS10)| (1 << WGM12)| (1 << WGM13);;	// prescaler 1, start PWM
    ICR1 = pwmtop;
	DDRB |= (1 << DDB1);					// PB1+PB2 output. this enables output driver
	DDRB |= (1 << DDB2);

	// TIMER2: ~3.9kHz Fast PWM, 8bit
    OCR2 = 0;    							// PWM to zero
    TCCR2 |= (1 << COM21)|(1 << COM20);    	// inverting
    TCCR2 |= (1 << WGM21) | (1 << WGM20);   // fast PWM, 8bit
    TCCR2 |= (1 << CS20);    				// prescaler 1, start PWM
	DDRB |= (1 << DDB3);    				// PB3 output. this enables output driver


}
