#include "serial_ss.h"

#define PIN_CLOCK1			PD7	//
#define PIN_CLOCK2			PB2	// SS
#define PIN_DATA_ENABLE		PB4	// MISO
#define PIN_DATA			PB3	// MOSI
#define DDR_CLOCK1			DDRD
#define DDR_CLOCK2			DDRB
#define DDR_DATA_ENABLE		DDRB
#define DDR_DATA			DDRB
#define PORT_CLOCK1			PORTD
#define PORT_CLOCK2			PORTB
#define PORT_DATA_ENABLE	PORTB
#define PORT_DATA			PORTB

#define SET_CLOCK1()		PORT_CLOCK1 |= (1<<PIN_CLOCK1); _delay_us(1);
#define CLR_CLOCK1()		PORT_CLOCK1 &= ~(1<<PIN_CLOCK1); _delay_us(1);
#define SET_CLOCK2()		PORT_CLOCK2 |= (1<<PIN_CLOCK2); _delay_us(1);
#define CLR_CLOCK2()		PORT_CLOCK2 &= ~(1<<PIN_CLOCK2); _delay_us(1);
#define SET_DATA_ENABLE()	PORT_DATA_ENABLE |= (1<<PIN_DATA_ENABLE); _delay_us(1);
#define CLR_DATA_ENABLE()	PORT_DATA_ENABLE &= ~(1<<PIN_DATA_ENABLE); _delay_us(1);
#define SET_DATA()			PORT_DATA |= (1<<PIN_DATA); _delay_us(1);
#define CLR_DATA()			PORT_DATA &= ~(1<<PIN_DATA); _delay_us(1);
#define TOGGLE_DATA()		PORT_DATA ^= (1<<PIN_DATA); _delay_us(1);
#define CLOCK1_PULSE()		SET_CLOCK1(); _delay_us(1); CLR_CLOCK1(); _delay_us(1);
#define CLOCK2_PULSE()		SET_CLOCK2(); _delay_us(1); CLR_CLOCK2(); _delay_us(1);


void init_serial_ss(void) {
	uint8_t i;
	
	DDR_DATA |= (1<<PIN_DATA);
	DDR_CLOCK1 |= (1<<PIN_CLOCK1);
	DDR_CLOCK2 |= (1<<PIN_CLOCK2);
	DDR_DATA_ENABLE |= (1<<PIN_DATA_ENABLE);
	
	CLR_DATA();
	CLR_CLOCK1();		// standby state
	CLR_CLOCK2();		// standby state
	SET_DATA_ENABLE();	// inverted logic, DATA disabled
	

	/* clear both displays by writing 1 startbit (H) and 36 zeroes */
	
	CLR_DATA_ENABLE();	// enable DATA
	SET_DATA();			// startbit
	CLOCK1_PULSE();
	
	SET_DATA();
	int x;
	for(x = 0; x<3;x++) {
	for(i = 0; i < 100; i++) {
		CLOCK1_PULSE();
		CLOCK2_PULSE();
		
	}
	SET_DATA_ENABLE();
}

	return;
}
