#ifndef __SERIAL_SS_H__
#define __SERIAL_SS_H__

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

void init_serial_ss(void);
void serial_ss_left(uint8_t digits[3]);
void serial_ss_right(uint8_t digits[4]);

#endif // __SERIAL_SS_H__
