' DATA =   b5
' CLK =    b4
' STROBE = b3

$regfile = "attiny2313a.dat"
$crystal = 8000000

Config Portb.7 = Output                                     ' msg led

Config Portb.5 = Output                                     ' DATA
Config Portb.4 = Output                                     ' s
Config Portb.3 = Output                                     ' STROBE

Strobe Alias Portb.4
Clk Alias Portb.5
Dat Alias Portb.3
Msgled Alias Portb.7

Dim I As Byte
Dim Cmd As Byte

Strobe = 1
Clk = 1

Strobe = 0
Cmd = &H8F
Shiftout Dat , Clk , Cmd , 2
Strobe = 1

Strobe = 0
Cmd = &H44
Shiftout Dat , Clk , Cmd , 2
Strobe = 1

Strobe = 0
Cmd = &H00
Shiftout Dat , Clk , Cmd , 2
Strobe = 1


Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1
Strobe = 0
Cmd = &H55
Shiftout Dat , Clk , Cmd , 2
Strobe = 1


Do
  Waitms 100
  Msgled = 0
  Waitms 100
  Msgled = 1
Loop