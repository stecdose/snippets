#include <util/delay.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#define TX_INVERT 0
//#define F_CPU           8e6
#define BAUD            9600
#define STX_PORT        PORTD
#define STX_BIT         PD1
#define TX_SRAM         0
#define TX_FLASH        1


void sputchar( uint8_t c )
{
  c = ~c;
#if !TX_INVERT
  STX_PORT &= ~(1<<STX_BIT);            // start bit
#else
  STX_PORT |= 1<<STX_BIT;
#endif
  for( uint8_t i = 10; i; i-- ){        // 10 bits
    _delay_us( 1e6 / BAUD );            // bit duration
#if !TX_INVERT
    if( c & 1 )
      STX_PORT &= ~(1<<STX_BIT);        // data bit 0
    else
      STX_PORT |= 1<<STX_BIT;           // data bit 1 or stop bit
#else
    if( c & 1 )
      STX_PORT |= 1<<STX_BIT;        // data bit 0
    else
      STX_PORT &= ~(1<<STX_BIT);           // data bit 1 or stop bit
    c >>= 1;
#endif
  }
}

void sputs( char *pt, uint8_t flag )
{
  uint8_t val;

  for(;;){
    if( flag )
      val = pgm_read_byte( pt );
    else
      val = *pt;
    if( val == 0 )
      return;
    sputchar( val );
    pt++;
  }
}
