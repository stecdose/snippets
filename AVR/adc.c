#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "adc.h"

volatile int selected_ch, next_in_list, max_in_list, chlist[3] = {ADC6, ADC7, ADCtemp}; 
volatile uint16_t adcvals[16];
volatile uint32_t summing_area, nsamples;

void adc_set_channel(int ch) {
	ADMUX &= ~((1<<MUX3)|(1<<MUX2)|(1<<MUX1)|(1<<MUX0));
	ADMUX |= ch;
	selected_ch = ch;
}

uint16_t adc_read(int ch) {
	uint16_t adcval;
	if(ADCSRA & (1<<ADIE)) {	// ADC configured t� interrupt mode?
    	return adcvals[ch];
	}

	adc_set_channel(ch);
	adcval = adc_read_avg(128);
	
	return adcval;
}

void adc_start_conversion(void) {
	ADCSRA |= _BV(ADSC);                // Start conversion

	return;
}

ISR(ADC_vect) {
	summing_area += ADC;
	nsamples++;
	if(nsamples==25) {
		summing_area /= 25;
		adcvals[selected_ch] = summing_area;
		if(next_in_list <= max_in_list) {
			adc_set_channel(next_in_list);
			next_in_list++;
		}
		if(next_in_list > max_in_list) {
			next_in_list = 0;
		}
		nsamples = 0;
	}

	return;
}

uint16_t adc_read_avg(int samples) {
	int count = 0;
	uint32_t temp = 0;;

	while(samples){
    	//ADCSRA |= _BV(ADSC);                // Start conversion
		adc_start_conversion();
    	while(!bit_is_set(ADCSRA,ADIF));    // Loop until conversion is complete
    	ADCSRA |= _BV(ADIF);                // Clear ADIF by writing a 1 (this sets the value to 0)
		temp += ADC;
		samples--;
		count++;
    }

	temp /= count;

    return (uint16_t)temp;
}

void adc_set_psc(int psc) {

}

void init_adc(int interrupts) {
	adc_set_channel(chlist[0]);
	next_in_list = 1;
	max_in_list = 2;

	ADMUX = (1<<REFS1)|(1<<REFS0);				// select 1.1V internal reference
    
	ADCSRA = (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);     // prescaler 32

	if(interrupts)
		ADCSRA |= (1<<ADIE);


	DIDR0 = (1<<ADC7)|(1<<ADC6);
	ADCSRA |= (1<<ADEN);
	
	return;
}



