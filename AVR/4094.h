#ifndef __4094_H__
#define __4094_H__
#include <avr/io.h>
#include <util/delay.h>

int set_anidelay4094(int d);
void init_sr4094(void);
void sr4094_sendbyte(uint8_t data);

#endif
