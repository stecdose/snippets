#include <avr/io.h>
#include <avr/interrupt.h>
#include "ssdisplay-4094.h"
#include "encoder.h"
#include "adc.h"
#include "LED.h"
#include "buttons.h"
#include "load.h"
#include "ssdisplay-4094.h"

static volatile uint16_t ticks=0;

volatile uint32_t _uptime =0;

extern volatile int8_t enc_delta;          // -128 ... 127
extern int8_t last;

void init_systimer(void) {
	TCCR0= (1<<CS01)|(1<<CS00);       	//  
    TIMSK= (1<<TOIE0);                	//  
}
 
ISR(TIMER0_OVF_vect) {
	int8_t new, diff;
	
	TCNT0 = 5;
	new = 0;
	
	uint8_t encoderpin = PIN_ENCODER;
	
	if(PHASE_A) new = 3;
	if(PHASE_B) new ^= 1;          		// convert gray to binary
	
	diff = last - new;               	// difference last - new
	
	if(diff & 1) {                 		// bit 0 = value (1)
		last = new;                    	// store new as next last
		enc_delta += (diff & 2) - 1;   	// bit 1 = direction (+/-)
	}
	  
	ticks++;
	
	if(((ticks%48)==0)&&is_hwb_pressed()) {
		load_toggle();
		lock_hwb = 1;
	}
	
	if(((ticks%51)==0)) {
		update_seven_segment_display();		
	}
	
	if(((ticks % 50) == 0) && (!(PINB&(1<<PB4)))) {
		encbtn_pressed = 1;
		
	} 
	//if(((ticks % 150) == 0) && ((PINB&(1<<PB4)))) {
		//encbtn_pressed = 0;
		//lock_encbtn = 0;
	//}
	
	
	if(((ticks%100)==0)) {
		fp_fader_next();
		displaytab[5] = (int16_t)OCR2;
		displaytab[6] = (int16_t)OCR1A;
		displaytab[7] = (int16_t)OCR1B;
		if(showlabels)
			showlabels--;
	}
	
	if((ticks%15)==0) {
		PORTD &= ~(1<<PD4);
	}
	if((ticks%500)==0) {
		adc_sel_ch(CHBAT);
		adc_read();
		displaytab[0] = adc_read_avg(4);
	}
	
	if((ticks%750)==0) {
		adc_sel_ch(CHTEMP);
		adc_read();
		displaytab[4] = (adc_read_avg(4)*25);
		
	}
	if((ticks%1000)==0) {
		lock_hwb = 0;
		lock_encbtn=0;
		encbtn_pressed=0;
		PORTD |= (1<<PD4);
		ticks = 0;
		_uptime++;
		if((_uptime <= 999)) {
			displaytab[13] = (int16_t)_uptime;
		} else {
			displaytab[13] = (int16_t)(_uptime/60);
		}
	}
	
	return;
} 
