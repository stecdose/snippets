#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "encoder.h"

#define PHASE_A     (PIND & 1<<PD2)
#define PHASE_B     (PIND & 1<<PD3)



void encoder_init(void) {
	DDRB |= 0x01;                      // Setup PB0 as output

	/* set PD2 and PD3 as input */ 
  	DDRD &=~ (1 << PD2);				/* PD2 and PD3 as input */ 
  	DDRD &=~ (1 << PD3);        
  	PORTD |= (1 << PD3)|(1 << PD2);   /* PD2 and PD3 pull-up enabled   */

    TCCR0= (1<<CS01);       			// Start Timer 0 with prescaler 8
    TIMSK= (1<<TOIE0);                	// Enable Timer 0 overflow interrupt


	return;
}

// Interrupt subroutine for Timer0 Overflow interrupt
ISR(TIMER0_OVF_vect) {
	static int ticks = 0;
 	if((ticks % 2000)==0) {
    	PORTD ^= (1<<DDB4); 
     }  

	 if((ticks % 200)==0) {
    	 
     }  
	ticks++;                
} 
 
