#include <avr/io.h>
#include "eeprom.h"
#include "uart.h"
#include "i2cmaster.h"

uint8_t eep2416_write(uint8_t eepaddr, uint16_t addr, uint8_t data) {
	uint8_t page = addr / 256;
	uint8_t off = addr % 256;
	i2c_start_wait((eepaddr|page)+I2C_WRITE);
	i2c_write(off);
	i2c_write(data);
	i2c_stop();
	
	return 0;
}

uint8_t eep2416_read(uint8_t eepaddr, uint16_t addr) {
	uint8_t ret;
	uint8_t page = addr / 256;
	uint8_t off = addr % 256;
	i2c_start_wait((eepaddr|page)+I2C_WRITE);
	i2c_write(off);
	i2c_rep_start((eepaddr|page)+I2C_READ);
	ret = i2c_readNak();
	i2c_stop();
	return ret;
}

//void dumpeep(void) {
	//uint16_t addr = 0;
	//uint8_t ret;
	//for(addr = 0; addr <= 2047; addr++) {
		////ret = eep2416_read(addr);
		//uart_putc(ret);
		//_delay_ms(1);
	//}
//}
