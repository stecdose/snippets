#ifndef __adc_H__
#define __adc_H__

#include <avr/io.h>
#include <util/delay.h>

#define WITHOUT_INT	0
#define WITH_INT	1

#define ADC7	(1<<MUX2)|(1<<MUX1)|(1<<MUX0)
#define ADC6	(1<<MUX2)|(1<<MUX1) 
#define ADC5	(1<<MUX2)| (1<<MUX0)
#define ADC4	(1<<MUX2) 
#define ADC3	(1<<MUX1)|(1<<MUX0)
#define ADC2	(1<<MUX1) 
#define ADC1    (1<<MUX0)
#define ADC0	0 
#define ADCtemp (1<<MUX3) 
#define ADCvbg	(1<<MUX3)|(1<<MUX2)|(1<<MUX1)
#define ADCgnd	(1<<MUX3)|(1<<MUX2)|(1<<MUX1)|(1<<MUX0)

#define ADC_CH_BATTERY_VOLTAGE	(ADC7)
#define ADC_CH_LOAD_VOLTAGE		(ADCgnd)
#define ADC_CH_LOAD_CURRENT		(ADCgnd)
#define ADC_CH_T1				(ADCtemp)
void adc_start_conversion(void);
void init_adc(int interrupts);
void adc_set_channel(int ch);
uint16_t adc_read(int ch);
uint16_t adc_read_avg(int nsamples);
#endif
