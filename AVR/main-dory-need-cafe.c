#include "7seg_mpx.h"
#include <avr/io.h>
#include <util/delay.h>

const static uint8_t msg_dory[4] = { SEG_B|SEG_C|SEG_D|SEG_E|SEG_G,
								SEG_C|SEG_D|SEG_E|SEG_G,
								SEG_E|SEG_G,
								SEG_B|SEG_C|SEG_D|SEG_F|SEG_G
							}; 


int main(void) {
	uint8_t data;
	int i;

	mpx_init();

	for(;;) {
		for(i = 0; i <= 3; i++) {
			data = msg_dory[i];
			mpx_set_digit(i);
			mpx_set_segments(data);
			
		}


	}
}

