/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.in by autoheader.  */

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <g15daemon_client.h> header file. */
#define HAVE_G15DAEMON_CLIENT_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `g15daemon_client' library
   (-lg15daemon_client). */
#define HAVE_LIBG15DAEMON_CLIENT 1

/* Define to 1 if you have the <libg15render.h> header file. */
#define HAVE_LIBG15RENDER_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have <sys/wait.h> that is POSIX.1 compatible. */
#define HAVE_SYS_WAIT_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Name of package */
#define PACKAGE "g15message"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "mlampard@users.sf.net"

/* Define to the full name of this package. */
#define PACKAGE_NAME "g15message.c"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "g15message.c 1.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "g15message-c"

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.0"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "1.0.0"
