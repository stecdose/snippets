#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <libg15.h>


int main(int argc, char *argv[]) {
    
//   // enable debug
//   if (debug != 0) {
//     libg15Debug(debug);
//     printf("G510s: debugging enabled, level %i\n", debug);
//   }
  
  // init libg15
  if (setupLibG15(0x46d, 0xc22d, 0) == G15_NO_ERROR) { // g510/g510s no audio
    printf("G510s: found device 046d:c22d\n");
  } else if (setupLibG15(0x46d, 0xc22e, 0) == G15_NO_ERROR) { // g510/g510s with audio
    printf("G510s: found device 046d:c22e\n");
  } else {
    printf("G510s: failed to initialize libg15\n");
    return -1;
  }

//   set_mkey_state(g510s_data.mkey_state);
    // media keys wont work without uinput
    if (init_uinput() != 0) {
      printf("G510s: failed to initialize uinput, media keys not available\n");
    }
// clear the screen
//     g15canvas *canvas = (g15canvas *)malloc(sizeof(g15canvas));
//     if(canvas == NULL) {
//       printf("G510s: failed to allocate clearing canvas\n");
//     } else {
//       memset(canvas->buffer, 0, G15_BUFFER_LEN);
//       g15r_clearScreen(canvas, 0);
//       if(writePixmapToLCD(canvas->buffer) != 0) {
//         printf("G510s: failed to clear lcd\n");
//       }
//       free(canvas);
//     }
    
    // shut off the lights
    if(setLEDs(0) < 0) {
      printf("G510s: failed to clear leds\n");
    }
    if(setG510LEDColor(255, 255, 0) < 0) {
      printf("G510s: failed to clear color\n");
    }
    
    exit_uinput();
    exitLibG15();
  }    
