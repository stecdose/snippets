#!/bin/sh

DIR="./MSDN"
echo " scanning $DIR/*.iso"
echo "#######################"

for file in `ls $DIR/*.iso`
do
    list_file=$file.file_list.txt
    echo "generating from file <$file>"
    echo "output file          <$list_file>"
    isoinfo -l -i $file > $list_file
done

echo "end."