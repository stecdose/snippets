#!/bin/sh

DIR="./MSDN"
echo " scanning $DIR/*.iso"
echo "#######################"

for file in `ls $DIR/*.iso`
do
    md5_file=$file.7z
    echo "generating from file <$file>"
    echo "output file          <$md5_file>"
    md5sum $file > $md5_file
    cat $md5_file
done

echo "end."
