#!/bin/sh

DIR="./MSDN"
echo " scanning $DIR/*.iso"
echo "#######################"

for file in `ls $DIR/*.iso`
do
    archive_file=$file.7z
    echo "generating from file <$file>"
    echo "output file          <$archive_file>"
    7z a $archive_file $file
done

echo "end."